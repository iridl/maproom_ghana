doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0')
  
  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(property='maproom:Sort_Id', content='a03')
    title Meteo Ghana Maprooms
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/localconfig/navmenu.json')
    link(rel='canonical', href='index.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
   
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#agriculture')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/%281%20Jan%201981%29/%2831%20Dec%202020%29/RANGE/X/-3.75/0.25/1.75/GRID/Y/4.25/0.25/11.75/GRID/dup/dup/c:/5.0/:c/%7Brainfall/ET%7Dds/dup/T/-1/shiftGRID/100.0/0.0/mul/rainfall/T/first/VALUE/T/removeGRID/0.0/mul/add/rainfall/.T/beginLoop/rainfall/add/ET/sub/60/min/0/max/endLoop/nip/dup/T/last/VALUE/T/1/shiftGRID/rainfall/add/ET/sub/60/min/0/max/appendstream/T/1/index/T/%281%20Apr%29//earlyCess/parameter/VALUES/.T/.gridvalues/%7B60//searchDaysCess/parameter/3//drySpellCess/parameter/add/%7Bdup/1/add/dup/last/gt/%7Bpop%7Dif%7Drepeat%7Dforall/VALUES/dup/T/npts//I/exch/NewIntegerGRID/replaceGRID/I//searchDaysCess/get_parameter//drySpellCess/get_parameter/add/1/add/splitstreamgrid/I//gridtype/0/def/pop/5//waterBalanceCess/parameter/flaglt/I/0/1/2/shiftdatashort/%5BI_lag%5Dsum//drySpellCess/get_parameter/flagge/1/masklt/1/index/.T/0.0/add//name//datesample/def/T/npts//I/exch/NewIntegerGRID/replaceGRID/I//searchDaysCess/get_parameter//drySpellCess/get_parameter/add/1/add/splitstreamgrid/I//gridtype/0/def/pop/exch/mul/%5BI%5Dminover/I2/3/-1/roll/T//earlyCess/get_parameter/VALUES/.T/2/array/astore/%7Bnpts%7Dforall/3/-1/roll/ne/%7Bfirst/secondtolast/subgrid%7Dif/dup//searchDaysCess/get_parameter/add/4/-3/roll/replaceGRID/exch/replaceNaN/3/-1/roll/T/%281%20Nov%29//earlyStart/parameter/60//searchDays/parameter/1//rainDay/parameter/5//runningDays/parameter/20//runningTotal/parameter/3//minRainyDays/parameter/7//dryDays/parameter/21//drySpell/parameter/onsetDate/1/RECHUNK/exch/T/.npts/2/index/.T/.npts/ne/%7BT/first/subgrid/2/index/.T/first/subgrid/sub/getrealization/0/get/0/lt/%7BT/second/last/RANGE%7D%7Bexch/T/first/secondtolast/RANGE/exch%7Difelse%7Dif/T/first/subgrid/2/index/.T/first/subgrid/sub/getrealization/0/get/0/lt/%7BT/second/last/RANGE/exch/T/first/secondtolast/RANGE/exch%7Dif/T/2/index/.T/replaceGRID/exch/%28Length%29//seasonStat/parameter/%28Length%29/ne/%7B4/-1/roll/dup/T/first/VALUE/1/index/.T/integrate/T//pointwidth/1/def/-0.5/shiftGRID/T//Ttemp/renameGRID/3/-1/roll/1/index%5BTtemp%5DBofA=C//units//mm/def/3/-2/roll%5BTtemp%5DBofA=C//units//mm/def%7Dif/sub//long_name/%28Season%20%29//seasonStat/get_parameter/append/def/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage/rainbowcolorscale/DATA/0/AUTO/RANGE%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7B%5BT%5Dstddev/0/masklt/Infinity/maskge/DATA/0/AUTO/RANGE%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7B//seasonStat/get_parameter/%28Length%29/eq/%7B90//probExcThresh1%7D%7B800//probExcThresh2%7Difelse/parameter//pET/parameter/flaggt%5BT%5Daverage/correlationcolorscale/%28/percent%29//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/%28/unitless%29/eq/%7B10.0/mul//units/%28years%20our%20of%2010%29/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE//long_name//pET/get_parameter/s==//seasonStat/get_parameter/%28Length%29/eq/%7B/%28%20days%20%29/%7D%7B/%28%20mm%20%29/%7Difelse/append/%28Season%20%29//seasonStat/get_parameter/append/append/def%7Dif//name//Season/def//long_name//yearlyStat/get_parameter/%28%20%29/append/long_name/append/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/1.65/RANGE/Y/4.345/11.625/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//framelabel%5B//earlyStart/get_parameter/%28%20Onset%20Window%2C%20%29//earlyCess/get_parameter/%28%20Cessation%20Window%20%29/%5Dconcat/psdef//antialias/true/psdef//layers%5B//Season//Districts//Regions%5Dpsdef+//antialias+true+psdef//plotaxislength+220+psdef//color_smoothing+1+psdef//plotborder+0+psdef+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

  body(xml:lang='<<<Language>>>')
    form#pageform(name='pageform')
      input.carryLanguage.carryup(name='Set-Language', type='hidden')

    .controlBar
      fieldset.navitem
        legend Data Library
        a.navlink.carryup(rev='section', href='/maproom/') Maproom

      fieldset.navitem
        legend Maproom
        span.navtext Climate and Agriculture
    div
      #content.searchDescription
        h2(property='term:title') Climate and Agriculture
        p(property='term:description')
          | The variability of seasonal precipitation, and the sub-seasonal statistics of these, play a key role in the quality and quantity of agricultural output.

        p
          | This Maproom includes maps and analyses of seasonal statistics of historical precipitation and seasonal precipitation forecasts
      |             
      .rightcol.tabbedentries(about='/maproom/Agriculture/')
        link(rel='maproom:tabterm', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Agriculture_Historical')

        a(rel='section' href='/python_maproom/onset/')
        div(about='/python_maproom/onset/')
          meta(property='maproom:Sort_Id' content='b1')
          a(rel='canonical' href='/python_maproom/onset/')
          p(property='term:title') Historical Onset Date
          p(property='term:description') The maproom explores historical rainy season onset dates based on user-defined definitions. The date when the rainy season starts is critical to agriculture planification, in particular for planting.
          link(rel='term:isDescribedBy' href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Agriculture_Historical')
          a(rel='term:icon' href='Onset.png')

    .optionsBar
      fieldset#share.navitem
        legend Share
