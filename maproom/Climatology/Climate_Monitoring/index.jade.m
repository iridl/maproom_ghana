=== title = "Dekadal Rainfall"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Monitoring')
    meta(xml:lang='', property='maproom:Sort_Id', content='a2')
    title <<<title>>>
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')

    link.share(rel='canonical', href='index.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rainfall')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#rainfall_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Climate_Indices')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standardized')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#StandardizedRainfallIndex')
    
    link(rel='term:icon', href='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/(rainfall)//monitAna/parameter/(rainfall)/eq/{.precip}if//name/(monitAna)/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//T/last/plotvalue//layers%5B//monitAna//<<<layers>>>%5Dpsdef+//plotaxislength+220+psdef//plotborder+0+psdef//color_smoothing+1+psdef+.gif')
    
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      .dlimage,
      .dlimage.wide {
      width: 46%;
      display: inline;
      }
      .dlimgts {
      width: 100%;
      }
      @media only all and (max-width: 800px) {
      .dlimage,
      .dlimage.wide {
      width: 95%;
      }
      }
      .dlimgtsbox {
      width: 85%;
      display: inline-block
      }
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="monitAna"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="monitAna"] #auxtopo {
      display: none;
      }

  body(xml:lang='<<<Language>>>')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carry.dlimg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.dlimg.dlauximg.share.bodyClass(name='monitAna', type='hidden', data-default='rainfall')
      input.dlimg.share(name='T', type='hidden')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.carry.pickarea.share.admin(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.share.dlimg.dlauximg.cumulOpt(name='startDay', type='hidden', data-default='<<<monit_startDay>>>')
      //input.share.dlimg.dlauximg.cumulOpt(name='endDay', type='hidden', data-default='last')
      input.dlimg.share.bodyAttribute(name='layers', value='monitAna', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Districts', checked='checked', type='checkbox')
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring')
          span(property='term:label') Climate Monitoring

      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')

      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')

      fieldset.navitem
        legend Analysis
        select.pageformcopy(name='monitAna')
          option(value='rainfall') Rainfall Amounts
          option(value='anom') Rainfall Anomalies
          option(value='anomper') Rainfall Anomalies in Percent
          option(value='cumulTot') Cumulative Rainfall
          option(value='cumul') Cumulative Anomalies
          option(value='cumulper') Cumulative Anomalies in Percent
          option(value='spi') SPI
      fieldset#startDay.navitem
        legend Cumulative Rainfall Since
        |     From
        input.pageformcopy(name='startDay', type='text', value='<<<monit_startDay>>>', size='6', maxlength='6')
        
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us

      fieldset.regionwithinbbox.dlimage.bis(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        br
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template(align='center')
            | Merged Station-Satellite Rainfall for 
            span.bold(class='iridl:long_name')
        br
        br

        .dlimgtsbox
          div a)
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/.precip/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/last/365.0/3.0/mul/sub/last/RANGE//long_name/(Dekadal%20Rainfall)/def/DATA/0/AUTO/RANGE/toNaN/%5BT%5DREORDER/dup/0/mul/T/fig%3A/deltabars/%3Afig//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//XOVY/null/psdef/+.gif')
        .dlimgtsbox
          div b)
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/.precipdiff/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/last/365.0/3.0/mul/sub/last/RANGE//scale_symmetric/true/def//long_name/(Rainfall%20%20Anomalies)/def/dup/T/fig-/colorbars2/-fig//plotborder/72/psdef//XOVY/null/psdef/+.gif')
        .dlimgtsbox
          div c)
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/.precip/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/a%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/3/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/2/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/1/sub/s%3D%3D/VALUES/%3Aa/4/%7B4/-1/roll/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/nip//fullname/exch/def%7Drepeat/3/index/.fullname/interp/1/Jan/2/index/julian_day/31/Dec/4/-1/roll/julian_day/dekadGRID/3/%7B4/-1/roll/T/2/index/replaceGRID/exch%7Drepeat/pop/4/-1/roll/1/index/.T/fig%3A/solid/thin/grey/line/hotpink/line/deepskyblue/line/medium/black/line/%3Afig//plotbordertop/25/psdef//plotborderbottom/62/psdef//antialias/true/def/+.gif')
        .dlimgtsbox
          div d)
          // Takes into account new cumulative start day and plots a plume of percentiles.
          img.dlimgts.cumulOpt.regionwithinbbox(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/.precip//long_name/(Cumulative%20Rainfall)/def/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/T/last/dup/365/sub/exch/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/3/-1/roll/dup/T/(1%20Jan%201991)/(31%20Dec%201991)/RANGE/exch/T/(1%20Jan%201991)/(31%20Dec%202020)/RANGE/T/name/npts/NewIntegerGRID/replaceGRID/T/36/splitstreamgrid/T/2/index/T/dekadaledgesgrid/partialgrid/2/1/roll/pop/replaceGRID/T/(days%20since%201991-01-01)/streamgridunitconvert/T/T/dekadaledgesgrid/first/secondtolast/subgrid//calendar//365/def/gridS/365/store/modulus/pop/periodic/setgridtype/partialgrid/replaceGRID/nip/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/2/index/.T//pointwidth/11/def/replaceGRID/dup/%5BT2%5D0.05/0.95/0/replacebypercentile/DATA/0/AUTO/RANGE/a%3A/percentile/0.95/VALUE/percentile/removeGRID/%3Aa%3A/percentile/0.05/VALUE/percentile/removeGRID/%3Aa/3/-1/roll/%5BT2%5Daverage//fullname/(1991-2020%20Mean)/def/4/-1/roll//fullname/(Cumulative%20Rainfall)/def/DATA/0/AUTO/RANGE/T/fig-/grey/deltabars/medium/solid/black/line/blue/line/-fig//antialias/true/psdef//framelabel/(Grey%20shade%20covers%20area%20between%20the%205th%20and%2095th%20percentiles)/psdef//plotborder/100/psdef//antialias/true/psdef//fntsze/12/psdef/+.gif')
        br
        br
        p
          | a) Dekadal (i.e., ~10-daily) rainfall for the selected region over the last 3 years.
        p
          | b) Dekadal rainfall differences (from 1991-2020 mean) for the selected region over the last 3 years.
        p
          | c) Smoothed dekadal rainfall for the current year (thick black line) compared to previous years (blue-1 yr from present; pink- 2 yrs from present; grey-3 yrs from present).
        p
          | d) Cumulative dekadal rainfall (solid blue line) and the cumulative long-term average rainfall (solid black line) from user-chosen calendar day in the selected region. The grey plume indicates the range of the 5th and 95th cumulative rainfall percentiles.

      fieldset#content.dlimage.withMap(about='')
        a(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/(rainfall)//monitAna/parameter/(rainfall)/eq/{.precip}if//monitAna/get_parameter/(cumulTot)/eq/{.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/(Cumulative Rainfall)/def/DATA/0/AUTO/RANGE}if//monitAna/get_parameter/(cumul)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/(Cumulative Rainfall Anomaly)/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE}if//monitAna/get_parameter/(cumulper)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/(Cumulative Rainfall Anomaly in %25)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(anom)/eq/{.precipdiff}if//monitAna/get_parameter/(anomper)/eq/{.precipdiff_percent//long_name/(Rainfall Anomaly Expressed as %25 of Long-Term Average)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(spi)/eq/{<<<precip_spi>>>/DATA/-3.0/3.0/RANGE}if//name/(monitAna)/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//monitAna/get_parameter/(rainfall)/eq/{//framelabelstart/(Rainfall on )/psdef}if//monitAna/get_parameter/(cumulTot)/eq/{//framelabel/(Cumulative Rainfall on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumul)/eq/{//framelabel/(Cumulative Rainfall Anomaly on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumulper)/eq/{//framelabel/(Cumulative Rainfall Anomaly in Percent on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(anom)/eq/{//framelabelstart/(Rainfall Anomaly on )/psdef}if//monitAna/get_parameter/(anomper)/eq/{//framelabelstart/(Rainfall Anomaly in Percent on )/psdef}if//monitAna/get_parameter/(spi)/eq/{//framelabelstart/(Standard Rainfall Index 1-dekad on )/psdef}if//antialias/true/psdef//T/last/plotvalue//layers[//monitAna//<<<layers>>>]psdef/')
        img.dlimg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/(rainfall)//monitAna/parameter/(rainfall)/eq/{.precip}if//monitAna/get_parameter/(cumulTot)/eq/{.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/(Cumulative Rainfall)/def/DATA/0/AUTO/RANGE}if//monitAna/get_parameter/(cumul)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/(Cumulative Rainfall Anomaly)/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE}if//monitAna/get_parameter/(cumulper)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/(Cumulative Rainfall Anomaly in %25)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(anom)/eq/{.precipdiff}if//monitAna/get_parameter/(anomper)/eq/{.precipdiff_percent//long_name/(Rainfall Anomaly Expressed as %25 of Long-Term Average)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(spi)/eq/{<<<precip_spi>>>/DATA/-3.0/3.0/RANGE}if//name/(monitAna)/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//monitAna/get_parameter/(rainfall)/eq/{//framelabelstart/(Rainfall on )/psdef}if//monitAna/get_parameter/(cumulTot)/eq/{//framelabel/(Cumulative Rainfall on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumul)/eq/{//framelabel/(Cumulative Rainfall Anomaly on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumulper)/eq/{//framelabel/(Cumulative Rainfall Anomaly in Percent on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(anom)/eq/{//framelabelstart/(Rainfall Anomaly on )/psdef}if//monitAna/get_parameter/(anomper)/eq/{//framelabelstart/(Rainfall Anomaly in Percent on )/psdef}if//monitAna/get_parameter/(spi)/eq/{//framelabelstart/(Standard Rainfall Index 1-dekad on )/psdef}if//antialias/true/psdef//T/last/plotvalue//layers[//monitAna//<<<layers>>>]psdef/+.gif', border='0', alt='image')
        br
        span#auxvar
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.dekadal/(rainfall)//monitAna/parameter/(rainfall)/eq/{.precip}if//monitAna/get_parameter/(cumulTot)/eq/{.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/(Cumulative Rainfall)/def/DATA/0/AUTO/RANGE}if//monitAna/get_parameter/(cumul)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/(Cumulative Rainfall Anomaly)/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE}if//monitAna/get_parameter/(cumulper)/eq/{a%3A/.precip/T/last/365/sub/last/RANGE/T/(<<<monit_startDay>>>)//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/%3Aa%3A/.climatologies/.rfe_adj/%3Aa/T/4/-1/roll/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/(Cumulative Rainfall Anomaly in %25)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(anom)/eq/{.precipdiff}if//monitAna/get_parameter/(anomper)/eq/{.precipdiff_percent//long_name/(Rainfall Anomaly Expressed as %25 of Long-Term Average)/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE}if//monitAna/get_parameter/(spi)/eq/{<<<precip_spi>>>/DATA/-3.0/3.0/RANGE}if//name/(monitAna)/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//monitAna/get_parameter/(rainfall)/eq/{//framelabelstart/(Rainfall on )/psdef}if//monitAna/get_parameter/(cumulTot)/eq/{//framelabel/(Cumulative Rainfall on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumul)/eq/{//framelabel/(Cumulative Rainfall Anomaly on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(cumulper)/eq/{//framelabel/(Cumulative Rainfall Anomaly in Percent on (%25%3D[T]) from )/lpar/append//startDay/get_parameter/append/rpar/append/psdef}if//monitAna/get_parameter/(anom)/eq/{//framelabelstart/(Rainfall Anomaly on )/psdef}if//monitAna/get_parameter/(anomper)/eq/{//framelabelstart/(Rainfall Anomaly in Percent on )/psdef}if//monitAna/get_parameter/(spi)/eq/{//framelabelstart/(Standard Rainfall Index 1-dekad on )/psdef}if//antialias/true/psdef//T/last/plotvalue//layers[//monitAna//<<<layers>>>]psdef/+.auxfig/+.gif')
        span#auxtopo
          img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')

      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') <<<title>>>
        p: b Rainfall
        p: span(property='term:description')
          | This Analysis tool allows users to view different presentations of the the most recent dekad. 
        p  
          | The default map on this page displays dekadal (approximately 10-day) rainfall amounts over the country. The default map shows rainfall totals for the most recently available dekad, but totals for previous dekads can be displayed as well. By clicking a location on the map, the user can generate four time series graphs that provide analyses of recent rainfall averaged over an administrative district, with respect to that of recent years and the long-term mean.
        p: b Rainfall Anomaly
        p
          | The rainfall anomalies map displays the difference between the most recent dekadal rainfall and the long-term average (1991-2020). Positive (negative) values indicate dekadal rainfall that are above (below) the long-term mean or climatology.
        p: b Rainfall Anomaly in Percent
        p
          | The rainfall anomalies in percent map displays the difference between the most recent dekadal rainfall and the long-term average (1991-2020) expressed as a percentage of the climatology. Positive (negative) values indicate dekadal rainfall that are above (below) the long-term mean or climatology.
        p: b Cumulative Rainfall
        p
          | The cumulative rainfall map displays the accumulated rainfall from a user selected day.
        p: b Cumulative Anomaly
        p
          | The cumulative anomaly map displays the difference between the accumulated rainfall from a user selected day and the long-term average (1991-2020) for the same period. Positive (negative) values indicate accumulated rainfall that are above (below) the long-term mean or climatology.
        p: b Cumulative Anomaly in Percent
        p
          | The cumulative anomaly map displays the difference between the accumulated rainfall from a user selected day and the long-term average (1991-2020) for the same period expressed as a percentage of the climatology. Positive (negative) values indicate accumulated rainfall that are above (below) the long-term mean or climatology.
        p: b Standard Precipitation Index (SPI)
        p
          | The SPI map displays the standard rainfall index of the most recent dekadal rainfall (using 1991 to the latest complete year as base period). The SPI (McKee 1993) is the number of standard deviations that observed cumulative rainfall deviates from the climatological average. To compute the index, a long-term time series of rainfall accumulations over dekads are used to estimate an appropriate probability density function. The analyses shown here are based on the Pearson Type III distribution (i.e., 3-parameter gamma) as suggested by Guttman (1999). The associated cumulative probability distribution is then estimated and subsequently transformed to a normal distribution. The result is the SPI, which can be interpreted as a probability using the standard normal distribution (i.e., users can expect the SPI be within one standard deviation about 68% of the time, two standard deviations about 95% of the time, etc.) The analyses shown here utilize the FORTRAN code made available by Guttman (1999). Places where the dekadal climatology is less than 2 mm are masked out.
          table(border='1', cols='1')
            tr
              td
                center
                  pre.
                  table(border='1')
                    tr(align='center')
                      td
                        pre.
                          &nbsp;&nbsp;&nbsp;&nbsp;
                      td
                        b &nbsp;&nbsp;&nbsp;&nbsp; SPI Values &nbsp;&nbsp;&nbsp;&nbsp;
                      td(align='center')
                        b &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    tr
                      td(bgcolor='#14713D')
                        pre.
                      td(align='center') &ge; 2.00
                      td(align='center') Extremely Wet
                    tr
                      td(bgcolor='#3CB371')
                        pre.
                      td(align='center') 1.50 to 1.99
                      td(align='center') Severely Wet
                    tr
                      td(bgcolor='#98FB98')
                        pre.
                      td(align='center') 1.00 to 1.49
                      td(align='center') Moderately Wet
                    tr
                      td
                        pre.
                      td(align='center') -0.99 to 0.99
                      td(align='center') Near Normal
                    tr
                      td(bgcolor='#F5DEB3')
                        pre.
                      td(align='center') -1.00 to -1.49
                      td(align='center') Moderately Dry
                    tr
                      td(bgcolor='#D2691E')
                        pre.
                      td(align='center')  -1.50 to -1.99
                      td(align='center') Severely Dry
                    tr
                      td(bgcolor='#B22222')
                        pre.
                      td(align='center') &le; -2.00
                      td(align='center') Extremely Dry
            tr
              td
                table
                  tr
                    td
                      center
                        font(size='-2')
                          | Table adapted from McKee
                          i et al.
                          |  (1993)

      br
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km). The rainfall times series (<<<yearstadef>>> to present) were created by combining quality-controlled observations from stations with satellite rainfall estimates.

      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis

      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact
          a(href='mailto:<<<helpemail>>>?subject= <<<title>>> - <<<country_name>>>') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
          br
          
    .optionsBar
      fieldset#share.navitem
        legend Share

