=== title = "Seasonal SPI"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='seasonalSPI')
    meta(xml:lang='', property='maproom:Sort_Id', content='b2')
    title <<<title>>>
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')

    link.share(rel='canonical', href='seasonalspi.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rainfall')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Climate_Indices')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standardized')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#StandardizedRainfallIndex')

    link(rel='term:icon', href='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a:/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/%28Standardized%20Precipitation%20Index%29/def/:a:/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/%5BT2%5Daverage/1./3./div/flaggt/1./masklt/%5BT%5D/REORDER/CopyStream/:a/mul/DATA/-3/3/RANGE//name//spi/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//T/last/plotvalue//layers%5B//spi//<<<layers>>>/%5Dpsdef//plotborder+0+psdef//plotaxislength+220+psdef//color_smoothing+1+psdef+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="spi"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="spi"] #auxtopo {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Ghana:Districts:ds"] #notgridbox {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Ghana:Regions:ds"] #notgridbox {
      display: none;
      }

  body(xml:lang='<<<Language>>>')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryLanguage.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.dlimgloc.admin.share(name='bbox', type='hidden')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.dlimg.share(name='T', type='hidden')
      input.carry.dlimgts.dlimgloc.share(name='region', type='hidden')
      input.carry.share.admin.bodyAttribute(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.carry.dlimgts.share(name='plotrangeT1', type='hidden', data-default='<<<yearstadef>>>')
      input.carry.dlimgts.share(name='plotrangeT2', type='hidden')
      input.share.bodyAttribute.dlimg(name='layers', type='checkbox', value='spi' checked='checked')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Districts', checked='checked', type='checkbox')

    .controlBar
      fieldset#toSectionList.navitem
        legend <<<en:Maproom>>>
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') <<<en:Climate>>>

      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring')
          span(property='term:label') Climate Monitoring

      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')

      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')
      fieldset.navitem(style='float: right;')
        legend Local Plot Time Range
        input.pageformcopy(name='plotrangeT1', type='text', size='5', maxlength='5', value='<<<yearstadef>>>')
        |  to
        input.pageformcopy(name='plotrangeT2', type='text', size='5', maxlength='5')

    .ui-tabs
      ul.ui-tabs-nav
        li: a(href='#tabs-1') <<<en:Description>>>
        li: a(href='#tabs-2') <<<en:Dataset Documentation>>>
        li: a(href='#tabs-3') <<<en:Instructions>>>
        li: a(href='#tabs-4') <<<en:Contact Us>>>

      fieldset.regionwithinbbox.dlimage(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28<<<regiondef>>>%29//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            br
            span.bold(class='iridl:long_name')
        #notgridbox.valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(irids:SOURCES:Features:Political:Ghana:Districts:ds)/geoobject/(<<<regiondef>>>)//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near 
            span.bold(class='iridl:value')
        br
        img.dlimgts(rel='iridl:hasFigureImage', border='0', alt='image', src='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a%3A/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/(Standardized%20Precipitation%20Index)/def/%3Aa%3A/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/[T2]average/1./3./div/flaggt/1./masklt/[T]/REORDER/CopyStream/%3Aa/mul/DATA/-3/3/RANGE/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]weighted-average/T/(<<<yearstadef>>>)//plotrangeT1/parameter/last/cvsunits/4/4/getinterval//plotrangeT2/parameter/RANGE/dup/a-/-a/T/fig-/colorbars2/-fig//antialias/true/psdef/+.gif')
        br
        img.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a%3A/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/(Standardized%20Precipitation%20Index)/def/%3Aa%3A/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/[T2]average/1./3./div/flaggt/1./masklt/[T]/REORDER/CopyStream/%3Aa/mul/DATA/-3/3/RANGE/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]weighted-average/T/(<<<yearstadef>>>)//plotrangeT1/parameter/last/cvsunits/4/4/getinterval//plotrangeT2/parameter/RANGE/dup/a-/-a/T/fig-/colorbars2/-fig//antialias/true/psdef/+.auxfig/+.gif')

      fieldset#content.dlimage(about='')
        link(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a%3A/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/(Standardized%20Precipitation%20Index)/def/%3Aa%3A/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/%5BT2%5Daverage/1./3./div/flaggt/1./masklt/%5BT%5D/REORDER/CopyStream/%3Aa/mul/DATA/-3/3/RANGE//name//spi/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//T/last/plotvalue//layers%5B//spi//<<<layers>>>/%5Dpsdef/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', alt='image', src='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a%3A/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/(Standardized%20Precipitation%20Index)/def/%3Aa%3A/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/%5BT2%5Daverage/1./3./div/flaggt/1./masklt/%5BT%5D/REORDER/CopyStream/%3Aa/mul/DATA/-3/3/RANGE//name//spi/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//T/last/plotvalue//layers%5B//spi//<<<layers>>>/%5Dpsdef/+.gif')
        br
        img#auxvar.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.MON/.Rainfall/.monthly/.precip/a%3A/3/gamma3par/pcpn_accum/gmean/gsd/gskew/pzero/3/gammaprobs/3/gammastandardize/T//pointwidth/3/def//defaultvalue/%7Blast%7D/def/-1./shiftGRID/T/first/pointwidth/1/sub/add/last/RANGE//long_name/(Standardized%20Precipitation%20Index)/def/%3Aa%3A/T/3/runningAverage/T/12/splitstreamgrid/0./flaggt/%5BT2%5Daverage/1./3./div/flaggt/1./masklt/%5BT%5D/REORDER/CopyStream/%3Aa/mul/DATA/-3/3/RANGE//name//spi/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//T/last/plotvalue//layers%5B//spi//<<<layers>>>/%5Dpsdef/+.auxfig/+.gif')
        br
        img#auxtopo.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')

      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') <<<title>>>
        p(property='term:description') The Standardized Precipitation Index is an indicator of extreme rainfall events, whether they are drought or excess rainfall.

        p The SPI map displays the standard rainfall index of the most recent season of rainfall (using <<<yearstadef>>> to the latest complete year as base period). The SPI (McKee 1993) is the number of standard deviations that observed cumulative rainfall deviates from the climatological average. To compute the index, a long-term time series of rainfall accumulations over months are used to estimate an appropriate probability density function. The analyses shown here are based on the Pearson Type III distribution (i.e., 3-parameter gamma) as suggested by Guttman (1999). The associated cumulative probability distribution is then estimated and subsequently transformed to a normal distribution. The result is the SPI, which can be interpreted as a probability using the standard normal distribution (i.e., users can expect the SPI be within one standard deviation about 68% of the time, two standard deviations about 95% of the time, etc.) The analyses shown here utilize the FORTRAN code made available by Guttman (1999).
        p In seasons when it rains too little, the gamma distribution can not fit and a dry mask is applied.
        p Clicking on the map will pop-up a local time series at the 0.05˚ gridbox clicked. Users can also choose to spatially average the SPI over Districts, Regions or Countries, by clicking on the map or picking an administrative name in the drop-down list, once having chosen which level of administration. Users can also focus the time series on a small time period by using the menu in the Control Bar.
        table(border='1', cols='1')
          tr
            td
              center
                pre
                  table(border='1')
                    tr(align='center')
                      td
                        b Color Scale
                      td
                        b SPI Values
                      td(align='center')
                        b Category
                    tr
                      td(bgcolor='#14713D')
                        pre
                      td(align='center') = 2.00
                      td(align='center') Extremely Wet
                    tr
                      td(bgcolor='#3CB371')
                        pre
                      td(align='center') = 1.50 to 1.99
                      td(align='center') Severely Wet
                    tr
                      td(bgcolor='#98FB98')
                        pre
                      td(align='center') = 1.00 to 1.49
                      td(align='center') Moderately Wet
                    tr
                      td
                        pre
                      td(align='center') = -0.99 to 0.99
                      td(align='center') Near Normal
                    tr
                      td(bgcolor='#F5DEB3')
                        pre
                      td(align='center') = -1.00 to -1.49
                      td(align='center') Moderately Dry
                    tr
                      td(bgcolor='#D2691E')
                        pre
                      td(align='center') = -1.50 to -1.99
                      td(align='center') Severely Dry
                    tr
                      td(bgcolor='#B22222')
                        pre
                      td(align='center') = -2.00
                      td(align='center') Extremely Dry
          tr
            td
              table
                tr
                  td
                    center
                      font(size='-2')
                        | Table adapted from McKee
                        i et al.
                        |  (1993)

      br                  
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km). The rainfall times series (<<<yearstadef>>> to present) were created by combining quality-controlled observations from stations with satellite rainfall estimates.

      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis

      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact 
          a(href='mailto:<<<helpemail>>>?subject= <<<title>>> - <<<country_name>>>') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
          br

    .optionsBar
      fieldset#share.navitem
        legend Share
