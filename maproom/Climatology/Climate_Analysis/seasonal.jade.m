doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='en')
  
  head
    meta(name='viewport', content='width=device-width, initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Analysis')
    meta(xml:lang='', property='maproom:Sort_Id', content='a4')
    title Seasonal Climate Analysis
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link(rel='stylesheet', type='text/css', href='../../../localconfig/ui.css')
    link.share(rel='canonical', href='seasonal.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')
    link(rel='shortcut icon', href='../../icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jun)//seasonStart/parameter/(-)/append/(Aug)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul/%7Dif/T/last/VALUE/<<<features>>>/X/Y/fig-/colors/grey/verythin/stroke/black/thinnish/stroke/-fig//plotborder/0/psdef//antialias/true/psdef//plotaxislength/220/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='../../../localconfig/ui.js', xml:space='preserve')
   
    style(xml:space='preserve').
      .dlimage,
      .dlimage.wide {
      width: 46%;
      display: inline;
      }
      .dlimgts {
      width: 100%;
      }
      @media only all and (max-width: 800px) {
      .dlimage,
      .dlimage.wide {
      width: 95%;
      }
      }
      .dlauximg.bis {
      padding-left:30px;
      width: 89%;
      }
      
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="clim_var"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="clim_var"] #auxtopo {
      display: none;
      }
      .varprecip #pET2 {
      display: none;
      }
      .varprecip #pET3 {
      display: none;
      }
      .varprecip #pET4 {
      display: none;
      }
      .vartmax #pET1 {
      display: none;
      }
      .vartmax #pET3 {
      display: none;
      }
      .vartmax #pET4 {
      display: none;
      }
      .vartmin #pET1 {
      display: none;
      }
      .vartmin #pET2 {
      display: none;
      }
      .vartmin #pET4 {
      display: none;
      }
      .vartmean #pET1 {
      display: none;
      }
      .vartmean #pET2 {
      display: none;
      }
      .vartmean #pET3 {
      display: none;
      }
      .var #pET2 {
      display: none;
      }
      .var #pET3 {
      display: none;
      }
      .var #pET4 {
      display: none;
      }
      .yearlyStatMean #pET1 {
      display: none;
      }
      .yearlyStatStdDev #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }
      .yearlyStatMean #pET2 {
      display: none;
      }
      .yearlyStatStdDev #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }
      .yearlyStatMean #pET3 {
      display: none;
      }
      .yearlyStatStdDev #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }
      .yearlyStatMean #pET4 {
      display: none;
      }
      .yearlyStatStdDev #pET4 {
      display: none;
      }
      .yearlyStat #pET4 {
      display: none;
      }
      
      body[resolution="irids:SOURCES:Features:Political:Ghana:Districts:ds"] #notgridbox {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Ghana:Regions:ds"] #notgridbox {
      display: none;
      }
  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.share.dlimgloc.dlimglocclick(name='bbox', type='hidden')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', value='<<<gridres>>>')
      input.carry.share.ver2(name='YearStart', type='hidden', value='<<<yearstadef>>>')
      input.carry.share.ver2(name='YearEnd', type='hidden', value='<<<yearenddef>>>')
      input.carry.share.ver2(name='seasonStart', type='hidden', value='Jul')
      input.carry.share.ver2(name='seasonEnd', type='hidden', value='Sep')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='var', type='hidden', value='.precip')
      input.dlimg.dlauximg.share.carry.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', value='Mean')
      input.dlimg.share.pET(name='probExcThresh1', type='hidden', data-default='400.')
      input.dlimg.share.pET(name='probExcThresh2', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh3', type='hidden', data-default='20.')
      input.dlimg.share.pET(name='probExcThresh4', type='hidden', data-default='25.')
      input.dlimg.share.bodyAttribute(name='layers', value='clim_var', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Districts', checked='checked', type='checkbox')
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          span(property='term:label') Climate Analysis
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Variable
        select.pageformcopy(name='var')
          option(value='.precip') Rainfall
          option(value='.tmax') Maximum Temperature
          option(value='.tmin') Minimum Temperature
          option(value='.tmean') Mean Temperature
      fieldset#yearlyStat.navitem
        legend Seasonal Statistics
        select.pageformcopy(name='yearlyStat')
          option(value='Mean') Mean
          option(value='StdDev') Standard deviation
          option(value='PoE') Probability of exceeding
        span#pET1
          input.pageformcopy(name='probExcThresh1', type='text', value='400', size='3', maxlength='6')
          | mm
        span#pET2
          input.pageformcopy(name='probExcThresh2', type='text', value='30', size='3', maxlength='3')
          | &deg;C
        span#pET3
          input.pageformcopy(name='probExcThresh3', type='text', value='20', size='3', maxlength='3')
          | &deg;C
        span#pET4
          input.pageformcopy(name='probExcThresh4', type='text', value='25', size='3', maxlength='3')
          | &deg;C
      fieldset.navitem
        legend Season
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
      fieldset.navitem
        legend Period
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(ds)/rsearch/%7Bpop/pop/pop/geoobject/dup/parent/.dataset/.label/gid/iridl%3AgeoId/(%3Ads)/rsearch/%7Bpop/pop%7Dif/(%3Agid%40%25d%3Ads)/append/sprintf//name//region/def/use_as_grid%7D%7Binterp/c%3A/exch/%3Ac/grid%3A//name//region/def/values%3A/(<<<regiondef>>>)//region/parameter/%3Avalues/%3Agrid/addGRID//name//label/def%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
      
      fieldset.regionwithinbbox.dlimage.bis(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Analysis for 
            span.bold(class='iridl:long_name')
        img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%7Dif//fullname/(%20)//seasonStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/%7B/(%20Total%20Rainfall)/%7Dif//var/get_parameter/(.tmax)/eq/%7B/(%20Max%20Temperature)/%7Dif//var/get_parameter/(.tmin)/eq/%7B/(%20Min%20Temperature)/%7Dif//var/get_parameter/(.tmean)/eq/%7B/(%20Mean%20Temperature)/%7Dif/append/def/a%3A/%3Aa%3A/%3Aa%3A/%5BT%5Ddetrend-bfl/%3Aa/sub//fullname/(Linear%20trend)/def/T/fig%3A/solid/black/line/thinnish/red/line/%3Afig//plotaxislength/400/psdef//antialias/true/def//plotborderbottom/40/psdef/+.gif')
        br
        img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%7Dif/dup/%5BT%5Daverage/sub//var/get_parameter/(.precip)/eq/%7Bprcp_anomaly_max1000_colors2%7D%7Bsstacolorscale%7Difelse//name//clim_var/def/DATA/null/null/RANGE/a%3A//var/get_parameter/(.precip)/eq/%7B//long_name/(rainfall)/def%7Dif//var/get_parameter/(.tmax)/eq/%7B//long_name/(maximum%20temperature)/def%7Dif//var/get_parameter/(.tmin)/eq/%7B//long_name/(minimum%20temperature)/def%7Dif//var/get_parameter/(.tmean)/eq/%7B//long_name/(mean%20temperature)/def%7Dif/%3Aa%3A/%3Aa/T/fig-/colorbars2/zeroline/-fig//plotaxislength/400/psdef//framelabel/(%20)//seasonStart/get_parameter/append/(-)/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/%7B/(%20Rainfall)/%7Dif//var/get_parameter/(.tmax)/eq/%7B/(%20Max%20Temperature)/%7Dif//var/get_parameter/(.tmin)/eq/%7B/(%20Min%20Temperature)/%7Dif//var/get_parameter/(.tmean)/eq/%7B/(%20Mean%20Temperature)/%7Dif/append/(%20Anomalies)/append/psdef//fntsze/12/def//plotborderbottom/40/psdef/+.gif')
      fieldset.dlimage.withMap.ver2
        link(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul/%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//var/get_parameter/(.precip)/eq/%7B/400.//probExcThresh1/parameter/%7Dif//var/get_parameter/(.tmax)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//var/get_parameter/(.tmin)/eq/%7B/20.//probExcThresh3/parameter/%7Dif//var/get_parameter/(.tmean)/eq/%7B/25.//probExcThresh4/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap//percent/unitconvert/DATA/0/100/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//plotborder/72/psdef//plotaxislength/590/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers>>>/%5Dpsdef/')
        img.dlimg.bis.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul/%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//var/get_parameter/(.precip)/eq/%7B/400.//probExcThresh1/parameter/%7Dif//var/get_parameter/(.tmax)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//var/get_parameter/(.tmin)/eq/%7B/20.//probExcThresh3/parameter/%7Dif//var/get_parameter/(.tmean)/eq/%7B/25.//probExcThresh4/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap//percent/unitconvert/DATA/0/100/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//plotborder/72/psdef//plotaxislength/590/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers>>>/%5Dpsdef/+.gif', border='0', alt='image')
        br
        span#auxvar
          img.dlauximg.bis.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul/%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//var/get_parameter/(.precip)/eq/%7B/400.//probExcThresh1/parameter/%7Dif//var/get_parameter/(.tmax)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//var/get_parameter/(.tmin)/eq/%7B/20.//probExcThresh3/parameter/%7Dif//var/get_parameter/(.tmean)/eq/%7B/25.//probExcThresh4/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap//percent/unitconvert/DATA/0/100/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph>>>/-fig//antialias/true/psdef//plotborder/72/psdef//plotaxislength/590/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers>>>/%5Dpsdef/.auxfig//plotborder+72+psdef//plotaxislength+590+psdef+.gif')
        span#auxtopo
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/++.auxfig+//plotaxislength+590+psdef//plotborder+72+psdef//fntsze/20/psdef/+.gif')
      fieldset.regionwithinbbox.dlimage(about='')
        div
          img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%7Dif//long_name//var/get_parameter/(.precip)/eq/%7B/(Total%20Rainfall%20)/%7Dif//var/get_parameter/(.tmax)/eq/%7B/(Maximum%20Temperature)/%7Dif//var/get_parameter/(.tmin)/eq/%7B/(Minimum%20Temperature%20)/%7Dif//var/get_parameter/(.tmean)/eq/%7B/(Mean%20Temperature%20)/%7Dif/def/DATA/AUTO/AUTO/RANGE/%5BT%5D/0./99/%7B/dup/1./100./div/add/%7D/repeat/0/replacebypercentile/percentile/-1/mul/1/add//percent/unitconvert/use_as_grid/percentile/last/first/RANGE/a-/-a/percentile//long_name/(Exceedance%20Probability)/def/fig-/blue/medium/profile/-fig//framelabel/(%20)//seasonStart/get_parameter/append/(-)/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/%7B/(%20Rainfall)/%7Dif//var/get_parameter/(.tmax)/eq/%7B/(%20Max%20Temperature)/%7Dif//var/get_parameter/(.tmin)/eq/%7B/(%20Min%20Temperature)/%7Dif//var/get_parameter/(.tmean)/eq/%7B/(%20Mean%20Temperature)/%7Dif/append/(%20Probability%20of%20Exceedance)/append/psdef//plotborderbottom/40/psdef//fntsze/12/def/+.gif')
          br
          br
      
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Seasonal Climate Analysis
        p(property='term:description')
          | Rainfall time series (<<<yearstadef>>>-<<<yearenddef>>>) and temperature time series (<<<yearstadef>>>-<<<yearenddef>>>) reconstructed from station observations, remote sensing and other proxies. This interface allows users to view rainfall, maximum, minimum and mean temperature seasonal climatologies, anomalies and probability of exceedance.
      
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall and temperature data over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4 km of resolution).
          | The rainfall time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations with satellite rainfall estimates.
          | Minimum and maximum temperature time series (<<<yearstadef>>> to <<<yearenddef>>>) were generated by combining quality-controlled station observations with downscaled reanalysis product.
      
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis
      
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
    
    .optionsBar
      fieldset#share.navitem
        legend Share
