doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  
  head
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Analysis')
    meta(property='maproom:Sort_Id', content='a5')
    title Seasonal Trend Analysis
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')

    link(rel='canonical', href='Seasonal_Trend.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jun)//seasonStart/parameter/(-)/append/(Aug)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul/%7Dif/T/last/VALUE/<<<features>>>/X/Y/fig-/colors/<<<graph>>>/-fig//plotborder/0/psdef//antialias/true/psdef//plotaxislength/220/psdef/+.gif')
    
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    
    style(xml:space='preserve').
      .dlimage,
      .dlimage.wide {
      width: 46%;
      display: inline;
      }
      .dlimgts {
      width: 100%;
      }
      @media only all and (max-width: 800px) {
      .dlimage,
      .dlimage.wide {
      width: 95%;
      }
      }
      .dlauximg.bis {
      padding-left: 30px;
      width: 89%;
      }
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display: none;
      }
      body[layers~="Trend"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="Trend"] #auxtopo {
      display: none;
      }
      
  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryLanguage.carryup.carry(name='Set-Language', type='hidden')
      input.carryup.carry.dlimg.dlimgloc.share(name='bbox', type='hidden')
      input.carry.dlimgts.dlimgloc.share(name='region', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', value='<<<gridres>>>')
      input.carry.share.ver2(name='YearStart', type='hidden', value='<<<yearstadef>>>')
      input.carry.share.ver2(name='YearEnd', type='hidden', value='<<<yearenddef>>>')
      input.carry.share.ver2(name='seasonStart', type='hidden', value='Jul')
      input.carry.share.ver2(name='seasonEnd', type='hidden', value='Sep')
      input.dlimg.dlauximg.share.dlimgts.bodyClass(name='var', type='hidden', value='.precip')
      input.dlimg.dlauximg.share.carry.bodyClass(name='trendUnits', type='hidden', value='perYear')
      input.dlimg.share.bodyAttribute(name='layers', value='Trend', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Districts', checked='checked', type='checkbox')

    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          span(property='term:label') Climate Analysis
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Variable
        select.pageformcopy(name='var')
          option(value='.precip') Rainfall
          option(value='.tmax') Maximum Temperature
          option(value='.tmin') Minimum Temperature
          option(value='.tmean') Mean Temperature
      fieldset#trendUnits.navitem
        legend Trend Units
        select.pageformcopy(name='trendUnits')
          option(value='perYear') Trend per year
          option(value='overPeriod') Trend over the period
          option(value='percPeriod') Trend in percentage
      fieldset.navitem
        legend Season
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
      fieldset.navitem
        legend Period
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(ds)/rsearch/%7Bpop/pop/pop/geoobject/dup/parent/.dataset/.label/gid/iridl%3AgeoId/(%3Ads)/rsearch/%7Bpop/pop%7Dif/(%3Agid%40%25d%3Ads)/append/sprintf//name//region/def/use_as_grid%7D%7Binterp/c%3A/exch/%3Ac/grid%3A//name//region/def/values%3A/(<<<regiondef>>>)//region/parameter/%3Avalues/%3Agrid/addGRID//name//label/def%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')

    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us

      fieldset.dlimage.withMap.ver2(about='')
        link(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/( )/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/{//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/{1/add}{13/add}ifelse/mul}if//fullname/( )//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/{/( Total Rainfall)/}if//var/get_parameter/(.tamx)/eq/{/( Max Temperature)/}if//var/get_parameter/(.tmin)/eq/{/( Min Temperature)/}if//var/get_parameter/(.tmean)/eq/{/( Mean Temperature)/}if/append/def/dup/dataflag/1.0/add/0/flagge/T/name/npts/NewIntegerGRID/replaceGRID/T/integral/T/second/last/RANGE/T/2/index/T/exch/pop/replaceGRID/dup/[T]average/2/index/[T]average/2/index/[T]stddev/dup/mul/4/index/[T]stddev/dup/mul/4/index/4/index/sub/6/index/4/index/sub/mul/dup/dataflag/[T]sum/dup/1/sub/3/-1/roll/[T]sum/exch/div/dup/4/index/div//fullname/(slope)/def//long_name/(Trend)/def/8/-7/roll/pop/pop/pop/pop/pop/pop/pop/(perYear)//trendUnits/parameter/(perYear)/eq/{exch/pop}if//trendUnits/get_parameter/(overPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/nip}if//trendUnits/get_parameter/(percPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/exch[T]average//var/get_parameter/(.precip)/eq/{1.0/add}if/div//percent/unitconvert//var/get_parameter/(.precip)/eq/{DATA/-100/100/RANGE}{DATA/-30/30/RANGE}ifelse}if/rainbowcolorscale//name//Trend/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel/( )//trendUnits/get_parameter/(perYear)/eq/{/(Change/year )/append}if//trendUnits/get_parameter/(overPeriod)/eq/{/(Change over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if//trendUnits/get_parameter/(percPeriod)/eq/{/(Change in %25 over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if/( for )/append//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/psdef//layers[//Trend//<<<layers>>>]psdef/')
        img.dlimg.bis.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/( )/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/{//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/{1/add}{13/add}ifelse/mul}if//fullname/( )//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/{/( Total Rainfall)/}if//var/get_parameter/(.tamx)/eq/{/( Max Temperature)/}if//var/get_parameter/(.tmin)/eq/{/( Min Temperature)/}if//var/get_parameter/(.tmean)/eq/{/( Mean Temperature)/}if/append/def/dup/dataflag/1.0/add/0/flagge/T/name/npts/NewIntegerGRID/replaceGRID/T/integral/T/second/last/RANGE/T/2/index/T/exch/pop/replaceGRID/dup/[T]average/2/index/[T]average/2/index/[T]stddev/dup/mul/4/index/[T]stddev/dup/mul/4/index/4/index/sub/6/index/4/index/sub/mul/dup/dataflag/[T]sum/dup/1/sub/3/-1/roll/[T]sum/exch/div/dup/4/index/div//fullname/(slope)/def//long_name/(Trend)/def/8/-7/roll/pop/pop/pop/pop/pop/pop/pop/(perYear)//trendUnits/parameter/(perYear)/eq/{exch/pop}if//trendUnits/get_parameter/(overPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/nip}if//trendUnits/get_parameter/(percPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/exch[T]average//var/get_parameter/(.precip)/eq/{1.0/add}if/div//percent/unitconvert//var/get_parameter/(.precip)/eq/{DATA/-100/100/RANGE}{DATA/-30/30/RANGE}ifelse}if/rainbowcolorscale//name//Trend/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel/( )//trendUnits/get_parameter/(perYear)/eq/{/(Change/year )/append}if//trendUnits/get_parameter/(overPeriod)/eq/{/(Change over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if//trendUnits/get_parameter/(percPeriod)/eq/{/(Change in %25 over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if/( for )/append//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/psdef//layers[//Trend//<<<layers>>>]psdef/+.gif', border='0', alt='image')
        br(clear='none')
        span#auxvar
          img.dlauximg.bis.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/( )/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/{//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/{1/add}{13/add}ifelse/mul}if//fullname/( )//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/{/( Total Rainfall)/}if//var/get_parameter/(.tamx)/eq/{/( Max Temperature)/}if//var/get_parameter/(.tmin)/eq/{/( Min Temperature)/}if//var/get_parameter/(.tmean)/eq/{/( Mean Temperature)/}if/append/def/dup/dataflag/1.0/add/0/flagge/T/name/npts/NewIntegerGRID/replaceGRID/T/integral/T/second/last/RANGE/T/2/index/T/exch/pop/replaceGRID/dup/[T]average/2/index/[T]average/2/index/[T]stddev/dup/mul/4/index/[T]stddev/dup/mul/4/index/4/index/sub/6/index/4/index/sub/mul/dup/dataflag/[T]sum/dup/1/sub/3/-1/roll/[T]sum/exch/div/dup/4/index/div//fullname/(slope)/def//long_name/(Trend)/def/8/-7/roll/pop/pop/pop/pop/pop/pop/pop/(perYear)//trendUnits/parameter/(perYear)/eq/{exch/pop}if//trendUnits/get_parameter/(overPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/nip}if//trendUnits/get_parameter/(percPeriod)/eq/{//YearEnd/get_parameter/interp//YearStart/get_parameter/interp/sub/1.0/add/mul/exch[T]average//var/get_parameter/(.precip)/eq/{1.0/add}if/div//percent/unitconvert//var/get_parameter/(.precip)/eq/{DATA/-100/100/RANGE}{DATA/-30/30/RANGE}ifelse}if/rainbowcolorscale//name//Trend/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//framelabel/( )//trendUnits/get_parameter/(perYear)/eq/{/(Change/year )/append}if//trendUnits/get_parameter/(overPeriod)/eq/{/(Change over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if//trendUnits/get_parameter/(percPeriod)/eq/{/(Change in %25 over )/append//YearStart/get_parameter/append/( - )/append//YearEnd/get_parameter/append}if/( for )/append//seasonStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/psdef//layers[//Trend//<<<layers>>>]psdef/+.auxfig/+.gif')
        span#auxtopo
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/++.auxfig+//plotaxislength+590+psdef//plotborder+72+psdef//fntsze/20/psdef/+.gif')

      fieldset.dlimage
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
            a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
            .template
              | Analysis for
              span.bold(class='iridl:long_name')
        img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%7Dif//fullname/(%20)//seasonStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append//var/get_parameter/(.precip)/eq/%7B/(%20Total%20Rainfall)/%7Dif//var/get_parameter/(.tmax)/eq/%7B/(%20Max%20Temperature)/%7Dif//var/get_parameter/(.tmin)/eq/%7B/(%20Min%20Temperature)/%7Dif//var/get_parameter/(.tmean)/eq/%7B/(%20Mean%20Temperature)/%7Dif/append/def/a%3A/%3Aa%3A/%3Aa%3A/%5BT%5Ddetrend-bfl/%3Aa/sub//fullname/(Linear%20trend)/def/T/fig%3A/solid/black/line/thinnish/red/line/%3Afig//plotaxislength/400/psdef//antialias/true/def//plotborderbottom/40/psdef/+.gif')

      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Seasonal Trend Analysis
        p(property='term:description')
          | Rainfall time series (<<<yearstadef>>>-<<<yearenddef>>>) and temperature time series (<<<yearstadef>>>-<<<yearenddef>>>) reconstructed from station observations, remote sensing and other proxies. This interface allows users to view rainfall, maximum, minimum and mean temperature seasonal trends expressed per year, over the selected period or percent of the average over the selected period.

      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall and temperature data over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4 km of resolution). The rainfall time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations with satellite rainfall estimates. Minimum and maximum temperature time series (<<<yearstadef>>> to <<<yearenddef>>>) were generated by combining quality-controlled station observations with downscaled reanalysis product.

      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis

      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.

    .optionsBar
      fieldset#share.navitem
        legend Share
