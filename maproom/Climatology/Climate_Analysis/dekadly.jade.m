doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  
  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Analysis')
    meta(xml:lang='', property='maproom:Sort_Id', content='a2')
    title Dekad Climate Analysis
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')

    link.share(rel='canonical', href='dekadly.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal/.c9120//var/get_parameter/interp/T/(1-10%20Sep)/VALUE//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//layers[//clim_var//<<<layers>>>/]psdef//plotborder/0/psdef//antialias/true/psdef//plotaxislength/220/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      .dlauximg.bis {
      padding-left:30px;
      width: 89%;
      }
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="clim_var"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="clim_var"] #auxtopo {
      display: none;
      }

  body(xml:lang='<<<Language>>>')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.dlimg.dlauximg.share(name='T', type='hidden', data-default='1-10 Jan')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.carry.pickarea.share.admin(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.carry.share.ver2(name='YearStart', type='hidden', data-default='<<<yearstadef>>>')
      input.carry.share.ver2(name='YearEnd', type='hidden', data-defautl='<<<yearenddef>>>')
      input.carry.share.ver2(name='seasonStart', type='hidden', data-default='Jul')
      input.carry.share.ver2(name='seasonEnd', type='hidden', data-default='Sep')
      input.carry.share.dlimg.dlauximg.dlimgts(name='var', type='hidden', data-default='.precip')
      input.dlimg.share.bodyAttribute(name='layers', value='clim_var', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Districts', checked='checked', type='checkbox')
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          span(property='term:label') Climate Analysis
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Variable
        select.pageformcopy(name='var')
          option(value='.precip') Rainfall
          option(value='.tmax') Maximum Temperature
          option(value='.tmin') Minimum Temperature
          option(value='.tmean') Mean Temperature
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')
      fieldset.navitem
        legend Yearly Seasonal Anomalies
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
          
      fieldset.regionwithinbbox.dlimage.bis(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')

        // <b>Monthly Precipitation Climatology</b>
        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal/.c9120//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]0.0/weighted-average/DATA/AUTO/AUTO/RANGE//var/get_parameter/(.precip)/eq/{//long_name/(rainfall)/def/dup/0.0/mul/T/fig-/grey/deltabars/-fig//framelabel/(Dekadal Precipitation Climatology)/psdef}{//long_name//var/get_parameter/(.tmax)/eq/{/(maximum temperature)/}if//var/get_parameter/(.tmin)/eq/{/(minimum temperature)/}if//var/get_parameter/(.tmean)/eq/{/(mean temperature)/}if/def/T/fig-/solid/thick/grey/line/-fig//framelabel//var/get_parameter/(.tmax)/eq/{/(Dekadal Maximum Temperature Climatology)/}if//var/get_parameter/(.tmin)/eq/{/(Dekadal Minimum Temperature Climatology)/}if//var/get_parameter/(.tmean)/eq/{/(Dekadal Mean Temperature Climatology)/}if/psdef}ifelse//plotborderbottom/40/psdef/+.gif')
        br
        br

        img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]weighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/( )/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/{//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/{1.0/add}{13.0/add}ifelse/3.0/mul/mul}if/dup/[T]average/sub//var/get_parameter/(.precip)/eq/{prcp_anomaly_max1000_colors2}{sstacolorscale}ifelse//name//ClimVar/def/DATA/AUTO/AUTO/RANGE/a%3A//var/get_parameter/(.precip)/eq/{//long_name/(rainfall)/def}if//var/get_parameter/(.tmax)/eq/{//long_name/(maximum temperature)/def}if//var/get_parameter/(.tmin)/eq/{//long_name/(minimum temperature)/def}if//var/get_parameter/(.tmean)/eq/{//long_name/(mean temperature)/def}if/%3Aa%3A/%3Aa/T/fig-/colorbars2/-fig//plotaxislength/400/psdef//var/get_parameter/(.precip)/eq/{//framelabel/(Yearly Seasonal Rainfall Anomalies)/psdef}if//var/get_parameter/(.tmax)/eq/{//framelabel/(Yearly Seasonal Max Temperature Anomalies)/psdef}if//var/get_parameter/(.tmin)/eq/{//framelabel/(Yearly Seasonal Min Temperature Anomalies)/psdef}if//var/get_parameter/(.tmean)/eq/{//framelabel/(Yearly Seasonal Mean Temperature Anomalies)/psdef}if//plotborderbottom/40/psdef/+.gif')

      fieldset#content.dlimage.withMap(about='')
        link(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//layers[//clim_var//<<<layers>>>/]psdef/T/first/plotvalue/')
        img.dlimg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//layers[//clim_var//<<<layers>>>/]psdef/T/first/plotvalue/+.gif', border='0', alt='image')
        br
        span#auxvar
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.dekadal/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//layers[//clim_var//<<<layers>>>/]psdef/T/first/plotvalue/+.auxfig/+.gif')
        span#auxtopo
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/++.auxfig+//plotaxislength+432+psdef//plotborder+72+psdef//fntsze/20/psdef/+.gif')
      
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Dekad Climate Analysis
        p(property='term:description')
          | Rainfall and temperature time series reconstructed from station observations and remote sensing proxies. This interface allows users to view rainfall, maximum, minimum and mean temperature dekadal climatologies and anomalies.
      
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall and temperature data over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4 km of resolution).
          | The rainfall time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations with satellite rainfall estimates.
          | Minimum and maximum temperature time series (<<<yearstadef>>> to <<<yearenddef>>>) were generated by combining quality-controlled station observations with downscaled reanalysis product.
      
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis
      
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
          br
                    
    .optionsBar
      fieldset#share.navitem
        legend Share
