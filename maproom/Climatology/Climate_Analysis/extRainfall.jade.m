doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='Extreme_Rainfall')
    meta(xml:lang='', property='maproom:Sort_Id', content='c1')
    title Extreme Rainfall Analysis
    
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link.share(rel='canonical', href='extRainfall.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/40.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28mm/day%29/def/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT/10.0//probExcThresh4/parameter%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq/3.0//probExcThresh5/parameter%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq/3.0//probExcThresh6/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28CoV%29/eq/%7BDATA/0/50/RANGE%7Dif//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//plotborder+0+psdef//color_smoothing+1+psdef//plotaxislength+220+psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="proba"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="proba"] #auxtopo {
      display: none;
      }
      .seasonalStatTotRain #pET2 {
      display: none;
      }
      .seasonalStatTotRain #pET3 {
      display: none;
      }
      .seasonalStatTotRain #pET4 {
      display: none;
      }
      .seasonalStatTotRain #pET5 {
      display: none;
      }
      .seasonalStatTotRain #pET6 {
      display: none;
      }
      .seasonalStatNumWD #pET1 {
      display: none;
      }
      .seasonalStatNumWD #pET3 {
      display: none;
      }
      .seasonalStatNumWD #pET4 {
      display: none;
      }
      .seasonalStatNumWD #pET5 {
      display: none;
      }
      .seasonalStatNumWD #pET6 {
      display: none;
      }
      .seasonalStatNumDD #pET1 {
      display: none;
      }
      .seasonalStatNumDD #pET2 {
      display: none;
      }
      .seasonalStatNumDD #pET4 {
      display: none;
      }
      .seasonalStatNumDD #pET5 {
      display: none;
      }
      .seasonalStatNumDD #pET6 {
      display: none;
      }
      .seasonalStatRainInt #pET1 {
      display: none;
      }
      .seasonalStatRainInt #pET2 {
      display: none;
      }
      .seasonalStatRainInt #pET3 {
      display: none;
      }
      .seasonalStatRainInt #pET5 {
      display: none;
      }
      .seasonalStatRainInt #pET6 {
      display: none;
      }
      .seasonalStatNumDS #pET1 {
      display: none;
      }
      .seasonalStatNumDS #pET2 {
      display: none;
      }
      .seasonalStatNumDS #pET3 {
      display: none;
      }
      .seasonalStatNumDS #pET4 {
      display: none;
      }
      .seasonalStatNumDS #pET6 {
      display: none;
      }
      .seasonalStatNumWS #pET1 {
      display: none;
      }
      .seasonalStatNumWS #pET2 {
      display: none;
      }
      .seasonalStatNumWS #pET3 {
      display: none;
      }
      .seasonalStatNumWS #pET4 {
      display: none;
      }
      .seasonalStatNumWS #pET5 {
      display: none;
      }
      .seasonalStat #pET2 {
      display: none;
      }
      .seasonalStat #pET3 {
      display: none;
      }
      .seasonalStat #pET4 {
      display: none;
      }
      .seasonalStat #pET5 {
      display: none;
      }
      .seasonalStat #pET6 {
      display: none;
      }
      .seasonalStatPerDA #yearlyStat {
      display: none;
      }

      .yearlyStatVar #pET1 {
      display: none;
      }
      .yearlyStatCoV #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }

      .yearlyStatVar #pET2 {
      display: none;
      }
      .yearlyStatCoV #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }

      .yearlyStatVar #pET3 {
      display: none;
      }
      .yearlyStatCoV #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }
      .yearlyStatVar #pET4 {
      display: none;
      }
      .yearlyStatCoV #pET4 {
      display: none;
      }
      .yearlyStat #pET4 {
      display: none;
      }
      .yearlyStatVar #pET5 {
      display: none;
      }
      .yearlyStatCoV #pET5 {
      display: none;
      }
      .yearlyStat #pET5 {
      display: none;
      }

      .yearlyStatVar #pET6 {
      display: none;
      }
      .yearlyStatCoV #pET6 {
      display: none;
      }
      .yearlyStat #pET6 {
      display: none;
      }
      
      body[resolution="irids:SOURCES:Features:Political:Ghana:Districts:ds"] #notgridbox {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Ghana:Regions:ds"] #notgridbox {
      display: none;
      }

  body(xml:lang='en')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.carry.dlimg.dlauximg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jul')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Sep')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='01')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='30')
      input.dlimg.dlauximg.share.dlimgts(name='wetThreshold', type='hidden', data-default='1.')
      input.dlimg.dlauximg.share.dlimgts(name='spellThreshold', type='hidden', data-default='5.')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='TotRain')
      input.dlimg.dlauximg.share.carry.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='PoE')
      input.dlimg.dlauximg.share.pET(name='probExcThresh1', type='hidden', data-default='40.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh2', type='hidden', data-default='30.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh3', type='hidden', data-default='30.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh4', type='hidden', data-default='10.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh5', type='hidden', data-default='3.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh6', type='hidden', data-default='3.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh7', type='hidden', data-default='20.')
      input.dlimg.share.bodyAttribute(name='layers', data-default='proba', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Districts', checked='checked', type='checkbox')
    // *********************
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate

      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          span(property='term:label') Climate Analysis

      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Season
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='01', size='2', maxlength='2')
        |          to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='30', size='2', maxlength='2')
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='TotRain') Total Rainfall
          option(value='NumWD') Number of Wet Days
          option(value='NumDD') Number of Dry Days
          option(value='RainInt') Rainfall Intensity
          option(value='NumDS') Number of Dry Spells
          option(value='NumWS') Number of Wet Spells
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='PoE') Probability of exceeding
          option(value='PonE') Probability of non-exceeding
          option(value='Var') Variance
          option(value='CoV') Coefficient of Variation
        span#pET1
          input.pageformcopy(name='probExcThresh1', type='text', value='40', size='3', maxlength='6')
          | mm
        span#pET2
          input.pageformcopy(name='probExcThresh2', type='text', value='30', size='3', maxlength='3')
          | days
        span#pET3
          input.pageformcopy(name='probExcThresh3', type='text', value='30', size='3', maxlength='3')
          | days
        span#pET4
          input.pageformcopy(name='probExcThresh4', type='text', value='10', size='3', maxlength='3')
          | mm/day
        span#pET5
          input.pageformcopy(name='probExcThresh5', type='text', value='3', size='3', maxlength='3')
          | spells
        span#pET6
          input.pageformcopy(name='probExcThresh6', type='text', value='3', size='3', maxlength='3')
          | spells
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option(class='iridl:values region@value label')
      fieldset.navitem
        legend Wet/Dry Day definition
        | Rainfall amount above/below
        input.pageformcopy(name='wetThreshold', type='text', value='1.', size='5', maxlength='5')
        | mm/day
      fieldset.navitem
        legend Wet/Dry Spell definition
        input.pageformcopy(name='spellThreshold', type='text', value='5', size='2', maxlength='2')
        | continuous wet/dry days
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us

      fieldset.regionwithinbbox.dlimage.bis
        a.dlimgts(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/(<<<regiondef>>>)//region/geoobject/%5BX/Y%5Dweighted-average/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//long_name/(Total%20Rainfall)/def//units//mm/def%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20below%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20above%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def%7Dif/units/long_name/3/-1/roll/grid%3A//name/(probability)/def//long_name/(probability%20of%20exceeding)/def//units//percent/def/0.01/99.99/2/copy/exch/sub/500.0/div/exch/%3Agrid/100.0/div/-2.0/mul/1.0/add/erfinv/1/index/%5BT%5Dstddev/mul/2.0/sqrt/mul/exch/%5BT%5Daverage/add/0/max/exch//long_name/exch/def/exch//units/exch/def//name//clim_var/def/probability/table%3A/2/%3Atable/')

        img.dlimgloc(style='display: inline-block; float: left;', src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')
        #notgridbox.valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3AGhana%3ADistricts%3Ads)/geoobject/(<<<regiondef>>>)//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near 
            span.bold(class='iridl:value')
        br
        br

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28<<<regiondef>>>%29//region/geoobject/long_name/3/-2/roll/%5BX/Y%5Dweighted-average/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//long_name/%28Total%20Rainfall%29/def//units//mm/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/%28Number%20of%20Wet%20Days%20-%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/%28Number%20of%20Dry%20Days%20-%20days%20below%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/%28Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//long_name/%28Number%20of%20Dry%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20below%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//long_name/%28Number%20of%20Wet%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20above%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif/units/long_name/3/-1/roll/grid://name/%28probability%29/def//long_name/%28probability%20of%20exceeding%29/def//units//percent/def/0.01/99.99/2/copy/exch/sub/500.0/div/exch/:grid/100.0/div/-2.0/mul/1.0/add/erfinv/1/index/%5BT%5Dstddev/mul/2.0/sqrt/mul/exch/%5BT%5Daverage/add/0/max/exch//long_name/exch/def/exch//units/exch/def//name//clim_var/def/a-/-a/probability/fig-/blue/medium/profile/-fig//antialias/true/psdef//framelabel/%5B/7/-1/roll/%28%20in%20%29//DayStart/get_parameter/%28%20%29//seasonStart/get_parameter/%28%20-%20%29//DayEnd/get_parameter/%28%20%29//seasonEnd/get_parameter/%5D/concat/psdef/+.gif')

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28<<<regiondef>>>%29//region/geoobject/long_name/3/-2/roll/%5BX/Y%5Dweighted-average/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//long_name/%28Total%20Rainfall%29/def//units//mm/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/%28Number%20of%20Wet%20Days%20-%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/%28Number%20of%20Dry%20Days%20-%20days%20below%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/%28Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//long_name/%28Number%20of%20Dry%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20below%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//long_name/%28Number%20of%20Wet%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20above%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append/def%7Dif/units/long_name/3/-1/roll/grid://name/%28probability%29/def//long_name/%28probability%20of%20exceeding%29/def/0.0001/0.9999/2/copy/exch/sub/500.0/div/exch/:grid/0.0/add/-2.0/mul/1.0/add/erfinv/probability/0.0/add//name//myinv/def/exch/use_as_grid/probability//stdvar/renameGRID/-1/mul/1/add/%5Bstdvar%5Dpartial//long_name/%28probability%29/def/stdvar/2/index/%5BT%5Dstddev/mul/2.0/sqrt/mul/3/-1/roll/%5BT%5Daverage/add/0./max/3/-1/roll//long_name/exch/def/3/-1/roll//units/exch/def/exch/%7B/clim_var/pdf/%7D/ds/a-/.clim_var/-a-/.pdf/-a/fig-/medium/blue/scatterline/-fig//antialias/true/psdef//framelabel/%5B/7/-1/roll/%28%20in%20%29//DayStart/get_parameter/%28%20%29//seasonStart/get_parameter/%28%20-%20%29//DayEnd/get_parameter/%28%20%29//seasonEnd/get_parameter/%5D/concat/psdef/+.gif')

      fieldset.dlimage.withMap
        a(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/40.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def/10.0//probExcThresh4/parameter%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units//spells/def/3.0//probExcThresh5/parameter%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units//spells/def/3.0//probExcThresh6/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/100/RANGE/transparent/black/navy/0/VALUE/navy/navy/2/bandmax/blue/blue/6/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/20/bandmax/PaleGreen/PaleGreen/25/bandmax/moccasin/moccasin/30/bandmax/yellow/yellow/40/bandmax/DarkOrange/DarkOrange/50/bandmax/red/red/60/bandmax/DarkRed/DarkRed/80/bandmax/brown/brown/100/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28CoV%29/eq/%7BDATA/0/50/RANGE%7Dif//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28Total%20Rainfall%29/%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%28Number%20of%20Wet%20Days%20-%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B/%28Number%20of%20Dry%20Days%20-%20days%20below%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%28Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%28Number%20of%20Dry%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20below%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%28Number%20of%20Wet%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20above%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/') visit site

        img.dlimg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/40.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def/10.0//probExcThresh4/parameter%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units//spells/def/3.0//probExcThresh5/parameter%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units//spells/def/3.0//probExcThresh6/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/100/RANGE/transparent/black/navy/0/VALUE/navy/navy/2/bandmax/blue/blue/6/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/20/bandmax/PaleGreen/PaleGreen/25/bandmax/moccasin/moccasin/30/bandmax/yellow/yellow/40/bandmax/DarkOrange/DarkOrange/50/bandmax/red/red/60/bandmax/DarkRed/DarkRed/80/bandmax/brown/brown/100/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28CoV%29/eq/%7BDATA/0/50/RANGE%7Dif//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28Total%20Rainfall%29/%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%28Number%20of%20Wet%20Days%20-%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B/%28Number%20of%20Dry%20Days%20-%20days%20below%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%28Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%28Number%20of%20Dry%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20below%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%28Number%20of%20Wet%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20above%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.gif')

        span#auxvar
          img.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/40.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def/10.0//probExcThresh4/parameter%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units//spells/def/3.0//probExcThresh5/parameter%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units//spells/def/3.0//probExcThresh6/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/100/RANGE/transparent/black/navy/0/VALUE/navy/navy/2/bandmax/blue/blue/6/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/20/bandmax/PaleGreen/PaleGreen/25/bandmax/moccasin/moccasin/30/bandmax/yellow/yellow/40/bandmax/DarkOrange/DarkOrange/50/bandmax/red/red/60/bandmax/DarkRed/DarkRed/80/bandmax/brown/brown/100/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28CoV%29/eq/%7BDATA/0/50/RANGE%7Dif//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28Total%20Rainfall%29/%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%28Number%20of%20Wet%20Days%20-%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDD%29/eq/%7B/%28Number%20of%20Dry%20Days%20-%20days%20below%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%28Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20%29//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%28Number%20of%20Dry%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20below%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%28Number%20of%20Wet%20Spells%20-%20%29//spellThreshold/get_parameter/s==/append/%28%20or%20more%20continuous%20days%20above%20%29/append//wetThreshold/get_parameter/s==/append/%28%20mm%29/append%7Dif/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.auxfig/+.gif')

        span#auxtopo
          img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Extreme Rainfall Analysis
        p(property='term:description')
          | The Maproom facilitates the exploration of the history of extreme monthly and seasonal rainfall characteristics.
        p
          | The user can define the Season and Seasonal Daily Statistics of interest and then map the probability of non-/exceeding a user-defined threshold. The map therefore illustrates how likely or unlikely the threshold is to be crossed. The user can also look at the variance or coefficient of variation of the seasonal quantity to get a sense of the range of variability across the years. Clicking on the map will then produce the probability of exceeding graph represented by a normal distribution. The graph gives a sense of the range of possible value for the seasonal characteristics of interest, including its extreme values. The different options are detailed below.
        p(align='left')
          b Season
          | :
          | Specify the start and end dates of the season , over which the rainfall characteristcs are to be computed over the full range of years of data available (<<<yearstadef>>>-<<<yearenddef>>>).
          br
          br
          b Wet/Dry Day/Spell Definitions
          | :
          | These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Total Rainfall
          | : total cumulative precipitation (in mm) falling over the season.
          br
          b Number of Wet Days
          | : the number of wet days (as defined above) during the season.
          br
          b Number of Dry Days
          | : the number of dry days (as defined above) during the season.
          br
          b Rainfall Intensity
          | : the average daily precipitation over the season considering only wet days.
          br
          b Number of Wet (Dry) Spells
          | : the number of wet (dry) spells during the season according to the definitions above. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
          br
          br
          b Yearly seasonal statistics
          | : choose the threshold to map the probability of non-/exceeding, or look at the variance or the coefficient of variation of the yearly seasonal time series across the <<<yearstadef>>>-<<<yearenddef>>> period.
          br
          b Probability of non-/exceeding
          | : the portion of years, in percentage, when the seasonal quantity exceeded or not a given threshold.
          br
          b Variance
          | : a measure of the variability of the seasonal quantity, expressed in square of the units. The higher, the farther the quantity departs from its mean value.
          br
          b Coefficient of Variation
          | : the ratio between the standard deviation (square root of the variance) and the mean of the seasonal quantity. It gives similar information as the variance but in perspective with the value of the mean rather than in units of the quantity.
          br
          br
          b Spatial Resolution
          | :
          | The analysis can be done and mapped at each <<<gridres>>>&ring; resolution grid point. Additionally it is possible to average the daily rainfall, prior to doing the analysis, over the <<<gridres>>>&ring; grid points falling within administrative boundaries for the probability of exceeding graph.
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall  on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km) from National Meteorology Agency. The time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations in NMA&rsquo;s archive with satellite rainfall estimates.
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
    br
    .optionsBar
      fieldset#share.navitem
        legend Share
