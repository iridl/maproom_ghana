<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.1"
      >

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />

  <title>Probability of Seasonal Temperature Conditioned on ENSO</title>
  <link rel="stylesheet" type="text/css" href="/maproom/<<<css_stem>>>.css" />
  <link class="share" rel="canonical" href="ENSO_Prob_Temp.html" />
  <link rel="home" href="<<<homeURL>>>" />
  <link rel="home alternate" type="application/json" href="/maproom/navmenu.json" class="carryLanguage"/>
  <meta xml:lang="" property="maproom:Sort_Id" content="z2"/>
  <meta xml:lang="" property="maproom:Entry_Id" content="ENSO_Prob_Temp" />

  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Prob_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
  <link rel="icon" href="/icons/<<<an_icon>>>" sizes="any" type="<<<an_icon_type>>>" />

  <link rel="term:icon" href="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale//var/get_parameter/1/4/getinterval/interp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE//name//proba/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//layers%5B//proba//<<<layers>>>/%5Dpsdef+//antialias+true+psdef//plotaxislength+182+psdef//color_smoothing+1+psdef//plotborder+0+psdef+.gif" />

  <script type="text/javascript" src="/uicore/uicore.js"></script>
  <script type="text/javascript" src="../../../localconfig/ui.js"></script>

 <style>
    .dlimgts.bis1 {
    margin-right:10px;
    padding-top:25px;
                  }

    .dlimgts.bis2 {
    padding-bottom:5px;
    text-align:left;
    padding-left:40px;
    width: 91%;
                  }

    body[layers] #auxvar {
    display: none;
                         }

    body[layers] #auxtopo {
    display:none;
                          }

    body[layers~="proba"] #auxvar {
    display: inline;
                                }

    body[layers~="bath"] #auxtopo {
    display: inline;
                                  }

    body[layers~="proba"] #auxtopo {
    display: none;
                                 }
  </style>
</head>

<body xml:lang="en">
  <form name="pageform" id="pageform">
    <input class="carry dlimg dlimgloc share dlimglocclick" name="bbox" type="hidden" />
    <input class="carry dlimg dlimgts share" name="season" type="hidden" data-default="Jan-Mar" />
    <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
    <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
    <input class="carry pickarea admin" name="resolution" type="hidden" data-default="<<<gridres>>>" />
    <input class="dlimg share" name="tercile" type="hidden" data-default="Cold" />
    <input class="dlimg share" name="ensoState" type="hidden" data-default="LaNina" />
    <input class="carry share dlimg dlauximg dlimgts" name="var" type="hidden" data-default=".tmin" />
    <input class="notused" name="plotaxislength" type="hidden" />

    <input class="dlimg share bodyAttribute" name="layers" value="proba" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="bath" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Districts" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Regions" checked="checked" type="checkbox" />
</form>

<div class="controlBar">
  <fieldset class="navitem" id="toSectionList">
    <legend>Maproom</legend>
    <a rev="section" class="navlink carryup" href="/maproom/Climatology/">Climate</a>
  </fieldset>

  <fieldset class="navitem" id="chooseSection">
    <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"><span property="term:label">Climate Analysis</span></legend>
  </fieldset>

  <fieldset class="navitem">
    <legend>Region</legend>
    <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/<<<regions_stem>>>.json"></a>
    <select class="RegionMenu" name="bbox">
      <option value=""><<<country_name>>></option>
      <optgroup class="template" label="Region">
        <option class="irigaz:hasPart irigaz:id@value term:label"></option>
      </optgroup>
    </select>
  </fieldset>

  <fieldset class="navitem"><legend>Variable</legend>
    <select class="pageformcopy" name="var">
      <option value=".tmin">tmin</option><option value=".tmax">tmax</option>
    </select>
  </fieldset>

  <fieldset class="navitem"><legend>Spatially Average Over</legend><span class="selectvalue"></span>
    <select class="pageformcopy" name="resolution">
      <option value="<<<gridres>>>">gridpoint</option>
      <option value="irids:SOURCES:Features:Political:Ghana:Districts:ds">Districts</option>
      <option value="irids:SOURCES:Features:Political:Ghana:Regions:ds">Regions</option>
    </select>

    <link class="admin" rel="iridl:hasJSON" href="/expert/%28<<<gridres>>>%29//resolution/parameter/dup/%28ds%29rsearch/%7Bpop/pop/pop/geoobject/dup/parent/.dataset/.label/gid/iridl:geoId/%28:ds%29rsearch/%7Bpop/pop%7Dif/%28:gid%40%25d:ds%29append/sprintf//name//region/def/use_as_grid%7D%7Binterp/c:/exch/:c/grid://name//region/def/values:/%28<<<regiondef>>>%29//region/parameter/:values/:grid/addGRID//name//label/def%7Difelse/info.json" />
    <select class="pageformcopy" name="region">
      <optgroup class="template" label="Label">
        <option class="iridl:values region@value label"></option>
      </optgroup>
    </select>
  </fieldset>

  <fieldset class="navitem"><legend>Season</legend><span class="selectvalue"></span><select class="pageformcopy" name="season"><option>Jan-Mar</option><option>Feb-Apr</option><option>Mar-May</option><option>Apr-Jun</option><option>May-Jul</option><option>Jun-Aug</option><option>Jul-Sep</option><option>Aug-Oct</option><option>Sep-Nov</option><option>Oct-Dec</option><option>Nov-Jan</option><option>Dec-Feb</option></select></fieldset>

  <fieldset class="navitem"><legend>Tercile</legend><span class="selectvalue"></span><select class="pageformcopy" name="tercile"><option>Cold</option><option>Normal</option><option>Hot</option></select></fieldset>

  <fieldset class="navitem"><legend>ENSO State</legend><span class="selectvalue"></span><select class="pageformcopy" name="ensoState"><option value="LaNina">La Niña</option><option value="Neutral">Neutral</option><option value="ElNino">El Niño</option></select></fieldset>
</div>

<div class="ui-tabs">
  <ul class="ui-tabs-nav">
    <li><a href="#tabs-1" >Description</a></li>
    <li><a href="#tabs-2" >Dataset Documentation</a></li>
    <li><a href="#tabs-3" >Instructions</a></li>
    <li><a href="#tabs-4" >Contact Us</a></li>
  </ul>

<fieldset class="regionwithinbbox dlimage bis" about="">
  <img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />

  <div class="valid" style="display: inline-block; text-align: top;">
    <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28<<<regiondef>>>%29//region/parameter/geoobject/info.json"></a>
    <div class="template" align="center">Observations for <span class="bold iridl:long_name"></span>
    </div>
  </div>
  
  <img class="dlimgts bis1" rel="iridl:hasFigureImage" src="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/%28<<<regiondef>>>%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jan-Mar%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Temperature%20Tercile%29/def/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28ENSO%20Phase%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/T/2/index/.T/a:/.first/:a:/.last/:a/RANGE/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/fig-/colorbars2/medium/solid/green/line/black/line/-fig//antialias/true/def/+.gif" />
  <br />

  <img class="dlimgts bis2" rel="iridl:hasFigureImage" src="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/%28<<<regiondef>>>%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jan-Mar%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Temperature%20Tercile%29/def/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28ENSO%20Phase%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/T/2/index/.T/a:/.first/:a:/.last/:a/RANGE/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/fig-/colorbars2/medium/solid/green/line/black/line/-fig//antialias/true/def/+.auxfig/+.gif" />
</fieldset>

<fieldset class="dlimage withMap" about="">
  <link rel="iridl:hasFigure" href="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale//var/get_parameter/1/4/getinterval/interp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE//name//proba/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/SOURCES/.Features/.Political/.Ghana/.Districts/.the_geom/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/X/Y/fig-/colors/colors/||/colors/black/verythin/stroke/thin/stroke/-fig//plotaxislength/600/psdef//antialias/true/psdef//layers%5B//proba//<<<layers>>>/%5Dpsdef/" />

  <img class="dlimg bis" rel="iridl:hasFigureImage" src="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale//var/get_parameter/1/4/getinterval/interp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE//name//proba/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/SOURCES/.Features/.Political/.Ghana/.Districts/.the_geom/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/X/Y/fig-/colors/colors/||/colors/black/verythin/stroke/thin/stroke/-fig//plotaxislength/600/psdef//antialias/true/psdef//layers%5B//proba//<<<layers>>>/%5Dpsdef/Tercile/last/plotvalue/ENSO/last/plotvalue+.gif" border="0" alt="image" />
  <br />

  <span id="auxvar">
    <img class="dlauximg bis" src="<<<ENACTSv>>>/.ALL/.Temperature/.monthly/%28.tmin%29//var/parameter/interp/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/%281856%29/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale//var/get_parameter/1/4/getinterval/interp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE//name//proba/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/SOURCES/.Features/.Political/.Ghana/.Districts/.the_geom/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/X/Y/fig-/colors/colors/||/colors/black/verythin/stroke/thin/stroke/-fig//plotaxislength/600/psdef//antialias/true/psdef//layers%5B//proba//<<<layers>>>/%5Dpsdef/Tercile/last/plotvalue/ENSO/last/plotvalue+.auxfig/+.gif" />
  </span>

  <span id="auxtopo">
    <img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/SOURCES/.Features/.Political/.Ghana/.Districts/.the_geom/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/X/Y/fig-/colors/-fig//antialias/true/psdef/++.auxfig+//plotaxislength+432+psdef//plotborder+72+psdef//fntsze/20/psdef/+.gif" />
  </span>
</fieldset>

<div id="tabs-1" class="ui-tabs-panel" about="">
  <h2 class="titre1"  property="term:title" >Probability of Seasonal Temperature Conditioned on ENSO</h2>
  <p property="term:description">This map shows the historical probability (given in percentile) of seasonal average monthly minimum or maximum temperature falling within the upper (hot), middle (normal), or bottom (cold) one-third ("tercile") of the <<<yearstadef>>>-<<<yearenddef>>> historical distribution in the country given the state of ENSO (El Niño, Neutral, La Niña) during the previous season (e.g. Jan-Mar temperature against Oct-Dec ENSO state).</p>

  <p>Here, the ENSO state for each season is defined according to the Oceanic Niño Index (ONI). It is calculated using Sea Surface Temperature (SST) anomalies, based on centered 30-year base periods updated every 5 years, in the geographical box (170˚W, 5˚S, 120˚W, 5˚N). A season is considered El Niño (La Niña) if it is part of at least 5 consecutive overlapping 3-month long seasons where the ONI is above 0.5˚C (below -0.5˚C). Use the controls on the page to select the season, rainfall tercile category of interest, and ENSO state. The analysis reproduces, using same SST dataset, the following <a href="http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/ensostuff/ensoyears.shtml">definition from NOAA.</a> </p>

  <p>Clicking on the map will then display, for the selected point, yearly seasonal minimum or maximum temperature averages time series. The colors of the bars depict what ENSO phase it was that year, and the horizontal lines show the historical terciles limits. This allows to quickly picture what years fell into what ENSO Phase and into what Temperature Tercile category.</p>

  <p>While the rainfall response to ENSO is nearly contemporaneous, this is not true for temperature. Once El Niño (La Niña) has begun, there is a ramp up (down) of global temperatures which are then slow to dissipate after the return to a neutral phase. Because of this we lag the temperature response to ENSO phases by 3 months.
  </p>

  <p>NB: This is not a forecast. It is based just on historical observations of minimum or maximum temperature and SST. However, it would be a good tool for exploring the effect of different ENSO phases on the following seasonal temperature.</p>

  <p>Reference for ENSO phases definition: <i> V. E. Kousky and R. W. Higgins, 2007: An Alert Classification System for Monitoring and Assessing the ENSO Cycle. Wea. Forecasting, 22, 353–371.
  doi: http://dx.doi.org/10.1175/WAF987.1</i></p>
</div>

<div id="tabs-2" class="ui-tabs-panel">
  <h2  class="titre1">Dataset Documentation</h2>
  <dl class="datasetdocumentation">Reconstructed temperature data over land areas on a <<<gridres>>>˚ x <<<gridres>>>˚ lat/lon grid (about 4 km of resolution). Minimum and maximum temperature time series (<<<yearstadef>>> to <<<yearenddef>>>) were generated by combining quality-controlled station observations with downscaled reanalysis product.
  </dl>
</div>

<div id="tabs-3" class="ui-tabs-panel">
  <h2  class="titre1">How to use this interactive map</h2>
  <p style="padding-right:25px;">
    <div class="buttonInstructions bis"></div>
  </p>
</div>

<div id="tabs-4" class="ui-tabs-panel">
  <h2 class="titre1">Helpdesk</h2>
  <p class="p1">Contact <a href="mailto:<<<helpemail>>>?subject=Historical Probability of Seasonal Average Temperature Tercile Conditioned on ENSO - NMA" target="_blank;"><<<helpemail>>></a> with any questions about or problems with this Map Room.
  </p>
</div>

</div>
<br/>

<div class="optionsBar">
  <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>

</body>
</html>
