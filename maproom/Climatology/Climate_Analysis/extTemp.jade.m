doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(xml:lang='', property='maproom:Entry_Id', content='Extreme_Temperature')
    meta(xml:lang='', property='maproom:Sort_Id', content='c2')
    title Extreme Temperature Analysis
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link.share(rel='canonical', href='extTemp.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')

    link(rel='shortcut icon', href='../../icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Temperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/%281991%29/%282020%29/RANGE/%28.tmin%29//var/parameter/interp/T/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/%28MeanTemp%29//seasonalStat/parameter/%28MeanTemp%29/eq/%7Bnip/seasonalAverage/20.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumHD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumCD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//plotborder+0+psdef//color_smoothing+1+psdef//plotaxislength+220+psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.      
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="proba"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="proba"] #auxtopo {
      display: none;
      }
      
      body[resolution="irids:SOURCES:Features:Political:Ghana:Districts:ds"] #notgridbox {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Ghana:Regions:ds"] #notgridbox {
      display: none;
      }

      .seasonalStatMeanTemp #pET2 {
      display: none;
      }
      .seasonalStatMeanTemp #pET3 {
      display: none;
      }
      .seasonalStatNumHD #pET1 {
      display: none;
      }
      .seasonalStatNumHD #pET3 {
      display: none;
      }
      .seasonalStatNumCD #pET1 {
      display: none;
      }
      .seasonalStatNumCD #pET2 {
      display: none;
      }

      .seasonalStat #pET2 {
      display: none;
      }
      .seasonalStat #pET3 {
      display: none;
      }

      .yearlyStatVar #pET1 {
      display: none;
      }
      .yearlyStatCoV #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }

      .yearlyStatVar #pET2 {
      display: none;
      }
      .yearlyStatCoV #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }

      .yearlyStatVar #pET3 {
      display: none;
      }
      .yearlyStatCoV #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }

  body(xml:lang='en')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.carry.dlimg.dlauximg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jul')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Sep')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='01')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='30')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='var', type='hidden', data-default='.tmin')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='MeanTemp')
      input.dlimg.dlauximg.share.carry.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='PoE')
      input.dlimg.dlauximg.share.dlimgts(name='hotThreshold', type='hidden', data-default='20.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh1', type='hidden', data-default='20.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh2', type='hidden', data-default='30.')
      input.dlimg.dlauximg.share.pET(name='probExcThresh3', type='hidden', data-default='30.')
      input.dlimg.share.bodyAttribute(name='layers', data-default='proba', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Regions', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Districts', checked='checked', type='checkbox')
    // *********************
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Climatology/') Climate

      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          span(property='term:label') Climate Analysis

      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Season
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='01', size='2', maxlength='2')
        |          to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='30', size='2', maxlength='2')
      fieldset.navitem
        legend Temperature
        select.pageformcopy(name='var')
          option(value='.tmin') Minimum Temperature
          option(value='.tmax') Maximum Temperature
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='MeanTemp') Seasonal Average
          option(value='NumHD') Number of Hot Days
          option(value='NumCD') Number of Cold Days
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='PoE') Probability of exceeding
          option(value='PonE') Probability of non-exceeding
          option(value='Var') Variance
          option(value='CoV') Coefficient of Variation
        span#pET1
          input.pageformcopy(name='probExcThresh1', type='text', value='20.', size='3', maxlength='6')
          | ˚C
        span#pET2
          input.pageformcopy(name='probExcThresh2', type='text', value='30', size='3', maxlength='3')
          | days
        span#pET3
          input.pageformcopy(name='probExcThresh3', type='text', value='30', size='3', maxlength='3')
          | days
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='irids:SOURCES:Features:Political:Ghana:Districts:ds') Districts
          option(value='irids:SOURCES:Features:Political:Ghana:Regions:ds') Regions
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option(class='iridl:values region@value label')
      fieldset.navitem
        legend Hot/Cold Day definition
        | Daily temperature above/below
        input.pageformcopy(name='hotThreshold', type='text', value='20.', size='5', maxlength='5')
        | ˚C
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
      fieldset.regionwithinbbox.dlimage
        a.dlimgts(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(1991)/(2020)/RANGE/(.tmin)//var/parameter/interp/(<<<regiondef>>>)//region/geoobject/[X/Y]weighted-average/T/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/(MeanTemp)//seasonalStat/parameter/(MeanTemp)/eq/{nip/seasonalAverage}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/pop/flexseasonalfreqGT/}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/pop/flexseasonalfreqLT/}if/units/long_name/3/-1/roll/grid://name/(probability)/def//long_name/(probability of exceeding)/def//units//percent/def/0.01/99.99/2/copy/exch/sub/500.0/div/exch/:grid/100.0/div/-2.0/mul/1.0/add/erfinv/1/index/[T]stddev/mul/2.0/sqrt/mul/exch/[T]average/add/exch//long_name/exch/def/exch//units/exch/def//name//clim_var/def/probability/table%3A/2/%3Atable/')

        img.dlimgloc(style='display: inline-block; float: left;', src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features>>>/X/Y/<<<littlegreymap>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')
        #notgridbox.valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3AGhana%3ADistricts%3Ads)/geoobject/(<<<regiondef>>>)//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near 
            span.bold(class='iridl:value')
        br
        br

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(1991)/(2020)/RANGE/(.tmin)//var/parameter/interp/(<<<regiondef>>>)//region/geoobject/long_name/3/-2/roll/[X/Y]weighted-average/T/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/(MeanTemp)//seasonalStat/parameter/(MeanTemp)/eq/{nip/seasonalAverage}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/pop/flexseasonalfreqGT/}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/pop/flexseasonalfreqLT/}if/units/long_name/3/-1/roll/grid%3A//name/(probability)/def//long_name/(probability%20of%20exceeding)/def//units//percent/def/0.01/99.99/2/copy/exch/sub/500.0/div/exch/%3Agrid/100.0/div/-2.0/mul/1.0/add/erfinv/1/index/[T]stddev/mul/2.0/sqrt/mul/exch/[T]average/add//seasonalStat/get_parameter/(MeanTemp)/ne/{/0./max}if/exch//long_name/exch/def/exch//units/exch/def//name//clim_var/def/a-/-a/probability/fig-/blue/medium/profile/-fig//antialias/true/psdef//framelabel/[/7/-1/roll/%28%20in%20%29//DayStart/get_parameter/(%20)//seasonStart/get_parameter/(%20-%20)//DayEnd/get_parameter/(%20)//seasonEnd/get_parameter/]/concat/psdef/+.gif')

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/%281991%29/%282020%29/RANGE/%28.tmin%29//var/parameter/interp/%28<<<regiondef>>>%29//region/geoobject/long_name/3/-2/roll/%5BX/Y%5Dweighted-average/T/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/%28MeanTemp%29//seasonalStat/parameter/%28MeanTemp%29/eq/%7Bnip/seasonalAverage%7Dif//seasonalStat/get_parameter/%28NumHD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqGT/%7Dif//seasonalStat/get_parameter/%28NumCD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqLT/%7Dif/units/long_name/3/-1/roll/grid://name/%28probability%29/def//long_name/%28probability%20of%20exceeding%29/def/0.0001/0.9999/2/copy/exch/sub/500.0/div/exch/:grid/0.0/add/-2.0/mul/1.0/add/erfinv/probability/0.0/add//name//myinv/def/exch/use_as_grid/probability//stdvar/renameGRID/-1/mul/1/add/%5Bstdvar%5Dpartial//long_name/%28probability%29/def/stdvar/2/index/%5BT%5Dstddev/mul/2.0/sqrt/mul/3/-1/roll/%5BT%5Daverage/add//seasonalStat/get_parameter/(MeanTemp)/ne/{/0./max}if/3/-1/roll//long_name/exch/def/3/-1/roll//units/exch/def/exch/%7B/clim_var/pdf/%7D/ds/a-/.clim_var/-a-/.pdf/-a/fig-/medium/blue/scatterline/-fig//antialias/true/psdef//framelabel/%5B/7/-1/roll/%28%20in%20%29//DayStart/get_parameter/%28%20%29//seasonStart/get_parameter/%28%20-%20%29//DayEnd/get_parameter/%28%20%29//seasonEnd/get_parameter/%5D/concat/psdef/+.gif')

      fieldset.dlimage.withMap
        a(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/%281991%29/%282020%29/RANGE/%28.tmin%29//var/parameter/interp/T/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/%28MeanTemp%29//seasonalStat/parameter/%28MeanTemp%29/eq/%7Bnip/seasonalAverage/20.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumHD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumCD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/50/RANGE/transparent/black/navy/0/VALUE/navy/navy/4/bandmax/blue/blue/8/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/16/bandmax/PaleGreen/PaleGreen/20/bandmax/moccasin/moccasin/24/bandmax/yellow/yellow/28/bandmax/DarkOrange/DarkOrange/32/bandmax/red/red/36/bandmax/DarkRed/DarkRed/40/bandmax/brown/brown/50/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//var/get_parameter/1/4/getinterval//seasonalStat/get_parameter/%28MeanTemp%29/eq/%7B/%28%20-%20Average%29/%7Dif//seasonalStat/get_parameter/%28MeanTemp%29/ne/%7B/%28%20-%20Number%20days%20%29//seasonalStat/get_parameter/%28NumHD%29/eq/%7B/%28above%20%29/%7D%7B/%28below%20%29/%7Difelse/append//hotThreshold/get_parameter/s==/append/%28%20%CB%9AC%29/append%7Dif/append/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/') visit site

        img.dlimg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/%281991%29/%282020%29/RANGE/%28.tmin%29//var/parameter/interp/T/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/%28MeanTemp%29//seasonalStat/parameter/%28MeanTemp%29/eq/%7Bnip/seasonalAverage/20.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumHD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumCD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/50/RANGE/transparent/black/navy/0/VALUE/navy/navy/4/bandmax/blue/blue/8/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/16/bandmax/PaleGreen/PaleGreen/20/bandmax/moccasin/moccasin/24/bandmax/yellow/yellow/28/bandmax/DarkOrange/DarkOrange/32/bandmax/red/red/36/bandmax/DarkRed/DarkRed/40/bandmax/brown/brown/50/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//var/get_parameter/1/4/getinterval//seasonalStat/get_parameter/%28MeanTemp%29/eq/%7B/%28%20-%20Average%29/%7Dif//seasonalStat/get_parameter/%28MeanTemp%29/ne/%7B/%28%20-%20Number%20days%20%29//seasonalStat/get_parameter/%28NumHD%29/eq/%7B/%28above%20%29/%7D%7B/%28below%20%29/%7Difelse/append//hotThreshold/get_parameter/s==/append/%28%20%CB%9AC%29/append%7Dif/append/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.gif')

        span#auxvar
          img.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/%281991%29/%282020%29/RANGE/%28.tmin%29//var/parameter/interp/T/%281%29//DayStart/parameter/%28%20%29/append/%28Jul%29//seasonStart/parameter/append/%28%20-%20%29/append/%2830%29//DayEnd/parameter/append/%28%20%29/append/%28Sep%29//seasonEnd/parameter/append/20//hotThreshold/parameter/0.0/%28MeanTemp%29//seasonalStat/parameter/%28MeanTemp%29/eq/%7Bnip/seasonalAverage/20.0//probExcThresh1/parameter%7Dif//seasonalStat/get_parameter/%28NumHD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqGT/30.0//probExcThresh2/parameter%7Dif//seasonalStat/get_parameter/%28NumCD%29/eq/%7B4/-1/roll/pop/flexseasonalfreqLT/30.0//probExcThresh3/parameter%7Dif/%28PoE%29//yearlyStat/parameter/%28CoV%29/eq/%7Bpop/dup%5BT%5Drmsaover/exch%5BT%5Daverage/div//percent/unitconvert/%28%20%29/exch%7Dif/startcolormap/DATA/0/50/RANGE/transparent/black/navy/0/VALUE/navy/navy/4/bandmax/blue/blue/8/bandmax/DeepSkyBlue/DeepSkyBlue/12/bandmax/aquamarine/aquamarine/16/bandmax/PaleGreen/PaleGreen/20/bandmax/moccasin/moccasin/24/bandmax/yellow/yellow/28/bandmax/DarkOrange/DarkOrange/32/bandmax/red/red/36/bandmax/DarkRed/DarkRed/40/bandmax/brown/brown/50/bandmax/brown/endcolormap//yearlyStat/get_parameter/%28Var%29/eq/%7Bpop/units/exch//units/undef%5BT%5Dstddev/Infinity/maskge/dup/mul/DATA/0/AUTO/RANGE/exch//units/exch/cvntos/%282%29/append/cvn/def/%28%20%29/exch%7Dif//yearlyStat/get_parameter/dup/%28PoE%29/eq/exch/%28PonE%29/eq/or/%7B//probExcThresh/parameter/exch/units/3/-2/roll/exch/flaggt%5BT%5Daverage//yearlyStat/get_parameter/%28PoE%29/ne/%7B-1/mul/1/add%7Dif//percent/unitconvert/correlationcolorscale/DATA/0/100/RANGE/exch/cvntos/%28%20%29/exch//probExcThresh/get_parameter/s==/exch/%28%20%29/exch/%28%20%29/5/array/astore/concat/exch%7Dif/exch//yearlyStat/get_parameter/exch/%28in%20%29/lpar//seasonStart/get_parameter/%28%20%29//DayStart/get_parameter/%28%20-%20%29//seasonEnd/get_parameter/%28%20%29//DayEnd/get_parameter/rpar/12/array/astore/concat//long_name/exch/def//name//proba/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features>>>/X/Y/fig-/colors/colors/||/colors/<<<graph>>>/-fig//antialias/true/psdef//framelabel//var/get_parameter/1/4/getinterval//seasonalStat/get_parameter/%28MeanTemp%29/eq/%7B/%28%20-%20Average%29/%7Dif//seasonalStat/get_parameter/%28MeanTemp%29/ne/%7B/%28%20-%20Number%20days%20%29//seasonalStat/get_parameter/%28NumHD%29/eq/%7B/%28above%20%29/%7D%7B/%28below%20%29/%7Difelse/append//hotThreshold/get_parameter/s==/append/%28%20%CB%9AC%29/append%7Dif/append/psdef//layers%5B//proba//<<<layers>>>%5Dpsdef/+.auxfig/+.gif')

        span#auxtopo
          img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Extreme Temperature Analysis
        p(property='term:description')
          | The Maproom facilitates the exploration of the history of extreme monthly and seasonal minimum and maximum temperatures.
        p
          | The user can define the Season over which to map the probability of minimum or maximum temperature no-to-/exceed a user-defined threshold. The map therefore illustrates how likely or unlikely the threshold is to be crossed. Clicking on the map will then produce the probability of exceeding graph represented by a normal distribution. The graph gives a sense of the range of possible value for monthly or seasonal minimum or maximum temperatures, including its extreme values. The different options are detailed below.
        p(align='left')
          b Season
          | :
          |             Specify the start and end dates of the season, over which the rainfall characteristcs are to be computed over the 1991-2020 climatological period.
          br
          br
          b Temperature
          | : Choose between minimum or maximum temperature.
          br
          br
          b Hot/Cold Day Definition
          | :
          | Threshold in ˚C above/below which a day is considered hot/cold.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Seasonal Average
          | : average daily temperature over the season.
          br
          b Number of Hot/Cold Days
          | : the number of hot/cold days (as defined above) during the season.
          br
          br
          b Yearly seasonal statistics
          | : choose the threshold to map the probability of non-/exceeding, or look at the variance or the coefficient of variation of the yearly seasonal time series across the 1991-2020 period.
          br
          b Probability of non-/exceeding
          | : the portion of years, in percentage, when the seasonal quantity exceeded or not a given threshold.
          br
          b Variance
          | : a measure of the variability of the seasonal quantity, expressed in square of the units. The higher, the farther the quantity departs from its mean value.
          br
          b Coefficient of Variation
          | : the ratio between the standard deviation (square root of the variance) and the mean of the seasonal quantity. It gives similar information as the variance but in perspective with the value of the mean rather than in units of the quantity.
          br
          br
          b Spatial Resolution
          | : The analysis can be done and mapped at each <<<gridres>>>&ring; resolution grid point. Additionally it is possible to average the daily min/max temperature, prior to doing the analysis, over the <<<gridres>>>&ring; grid points falling within administrative boundaries for the probability of exceeding graph.
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed temperature data on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km) from National Meteorology Agency. Monthly Minimum and maximum temperature time series (1991 to 2020) were generated by combining quality-controlled station observations with downscaled reanalysis product.
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
    br
    .optionsBar
      fieldset#share.navitem
        legend Share

