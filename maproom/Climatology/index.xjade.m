doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0')

  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    meta(property='maproom:Sort_Id', content='a01')
    title Meteo Ghana Map Room
    link.carryLanguage(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/localconfig/navmenu.json')
    link(rel='canonical', href='index.html')
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Rainfall/.dekadal/.precip/<<<features>>>/X/Y/fig-/colors/<<<graph>>>/blue/medium/rivers_gaz/-fig//antialias/true/psdef//T/last/plotvalue//plotaxislength/220/psdef//plotborder/0/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      div {
      padding-left:5px;
      padding-top:5px;
      text-align: justify;
      }
      img {
      padding-right:5px;
      padding-bottom:5px;
      }

  body(xml:lang='<<<Language>>>')
    form#pageform(name='pageform')
      input.carryLanguage.carryup(name='Set-Language', type='hidden')

    .controlBar
      fieldset.navitem
        legend Data Library
        a.navlink.carryup(rev='section', href='/maproom/') Maproom

      fieldset.navitem
        legend Maproom
        span.navtext Climate

    div
      #content.searchDescription
        h2(property='term:title') Climate
        p(property='term:description')
          | Climate affects sectors in society in a number of ways. These effects may be direct, as with heat stress, or indirect, as with infectious diseases such as malaria and meningitis.

        p
          | This facility aims to explore and inform users about the climate-society relationship with an emphasis on the seasonal nature of that relationship, where appropriate.

      .rightcol.tabbedentries(about='/maproom/Climatology/')
        link(rel='maproom:tabterm', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')

        a(rel='section' href='/python_maproom/monthly-climatology/')
        div(about='/python_maproom/monthly-climatology/')
          meta(property='maproom:Sort_Id' content='a3')
          a(rel='canonical' href='/python_maproom/monthly-climatology/')
          p(property='term:title') Monthly Climate Analysis
          p(property='term:description') This interface allows users to view rainfall, maximum, minimum and mean temperature monthly climatologies and anomalies.
          link(rel='term:isDescribedBy' href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis')
          a(rel='term:icon' href='monthly.png')
        | 
        link(rel='maproom:tabterm', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring')

    .optionsBar
      fieldset#share.navitem
        legend Share
