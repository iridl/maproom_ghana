doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    title Daily Temperature Analysis
    
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link.share(rel='canonical', href='daily_temp.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Daily_Temp_Analysis')
    meta(xml:lang='', property='maproom:Sort_Id', content='0a2')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Temperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/.tmax/T/11565.5/VALUE/<<<features_hydro>>>/X/Y/fig-/colors/<<<graph_hydro>>>/-fig//plotaxislength/220/psdef//antialias/true/psdef//plotborder/0/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')

    style.
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="clim_var"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="clim_var"] #auxtopo {
      display: none;
      }
      
      body[resolution='<<<water_sheds>>>'] #notgridbox {
      display: none;
      }
      .yearlyStat #pETall {
        display: none;
      }
      .yearlyStatMean #pETall {
        display: none;
      }
      .yearlyStatStdDev #pETall {
        display: none;
      }
      .yearlyStatCoV #pETall {
        display: none;
      }
      .seasonalStat #pET1 {
        display: none;
      }
      .seasonalStat #pET2 {
        display: none;
      }
      .seasonalStat #pET3 {
        display: none;
      }
      .seasonalStat #pET4 {
        display: none;
      }
      .seasonalStat #pET5 {
        display: none;
      }
      .seasonalStat #pET6 {
        display: none;
      }
      .seasonalStatMeanTemp #pET1 {
        display: none;
      }
      .seasonalStatMeanTemp #pET2 {
        display: none;
      }
      .seasonalStatMeanTemp #pET3 {
        display: none;
      }
      .seasonalStatMeanTemp #pET4 {
        display: none;
      }
      .seasonalStatMeanTemp #pET5 {
        display: none;
      }
      .seasonalStatMeanTemp #pET6 {
        display: none;
      }
      .seasonalStatMinTemp #pET0 {
        display: none;
      }
      .seasonalStatMinTemp #pET2 {
        display: none;
      }
      .seasonalStatMinTemp #pET3 {
        display: none;
      }
      .seasonalStatMinTemp #pET4 {
        display: none;
      }
      .seasonalStatMinTemp #pET5 {
        display: none;
      }
      .seasonalStatMinTemp #pET6 {
        display: none;
      }
      .seasonalStatMaxTemp #pET0 {
        display: none;
      }
      .seasonalStatMaxTemp #pET1 {
        display: none;
      }
      .seasonalStatMaxTemp #pET3 {
        display: none;
      }
      .seasonalStatMaxTemp #pET4 {
        display: none;
      }
      .seasonalStatMaxTemp #pET5 {
        display: none;
      }
      .seasonalStatMaxTemp #pET6 {
        display: none;
      }
      .seasonalStatCDD #pET0 {
        display: none;
      }
      .seasonalStatCDD #pET1 {
        display: none;
      }
      .seasonalStatCDD #pET2 {
        display: none;
      }
      .seasonalStatCDD #pET4 {
        display: none;
      }
      .seasonalStatCDD #pET5 {
        display: none;
      }
      .seasonalStatCDD #pET6 {
        display: none;
      }
      .seasonalStatNumCD #pET0 {
        display: none;
      }
      .seasonalStatNumCD #pET1 {
        display: none;
      }
      .seasonalStatNumCD #pET2 {
        display: none;
      }
      .seasonalStatNumCD #pET3 {
        display: none;
      }
      .seasonalStatNumCD #pET5 {
        display: none;
      }
      .seasonalStatNumCD #pET6 {
        display: none;
      }
      .seasonalStatNumHD #pET0 {
        display: none;
      }
      .seasonalStatNumHD #pET1 {
        display: none;
      }
      .seasonalStatNumHD #pET2 {
        display: none;
      }
      .seasonalStatNumHD #pET3 {
        display: none;
      }
      .seasonalStatNumHD #pET4 {
        display: none;
      }
      .seasonalStatNumHD #pET6 {
        display: none;
      }
      .seasonalStatGDD #pET0 {
        display: none;
      }
      .seasonalStatGDD #pET1 {
        display: none;
      }
      .seasonalStatGDD #pET2 {
        display: none;
      }
      .seasonalStatGDD #pET3 {
        display: none;
      }
      .seasonalStatGDD #pET4 {
        display: none;
      }
      .seasonalStatGDD #pET5 {
        display: none;
      }
      
  body(xml:lang='en')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.carry.dlimg.dlauximg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      //
        <input class="carry share locname" name="region" type="hidden" />
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', data-default='<<<gridres>>>')
      //
        <input class="pickarea" name="resolution" type="hidden" data-default='<<<water_sheds>>>' />
      input.carry.dlimg.dlauximg.share.dlimgts(name='YearStart', type='hidden', data-default='<<<yearstadef>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='YearEnd', type='hidden', data-default='<<<yearenddef>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jul')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Sep')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='01')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='30')
      input.dlimg.dlauximg.share.dlimgts(name='hotThreshold', type='hidden', data-default='0.')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='MeanTemp')
      input.dlimg.dlauximg.share.carry.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='Mean')
      input.dlimg.share.pET(name='probExcThresh0', type='hidden', data-default='0.')
      input.dlimg.share.pET(name='probExcThresh1', type='hidden', data-default='0.')
      input.dlimg.share.pET(name='probExcThresh2', type='hidden', data-default='0.')
      input.dlimg.share.pET(name='probExcThresh3', type='hidden', data-default='100.')
      input.dlimg.share.pET(name='probExcThresh4', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh5', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh6', type='hidden', data-default='100.')
      input.dlimg.dlauximg.carry.poeg.share.pET(name='poeunits', type='hidden', data-default='/percent')
      input.dlimg.share.bodyAttribute(name='layers', data-default='clim_var', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Water_Sheds', checked='checked', type='checkbox')
      input.dlimgts.share(name='plotrange1', type='hidden')
      input.dlimgts.share(name='plotrange2', type='hidden')
    // *********************
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Water/') Climate and Water
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
          span(property='term:label') Analysis
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Time Period (<<<yearstadef>>> to <<<yearenddef>>>)
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='01', size='2', maxlength='2')
        |          to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='30', size='2', maxlength='2')
        |         -
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='MeanTemp') Mean Temperature
          option(value='MinTemp') Minimum Temperature
          option(value='MaxTemp') Maximum Temperature
          option(value='CDD') Chilling Degree Days
          option(value='NumCD') Number of Cold Days
          option(value='NumHD') Number of Hot Days
          option(value='GDD') Growing Degree Days
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='Mean') Mean
          option(value='StdDev') Standard deviation
          option(value='CoV') Coefficient of Variation
          option(value='PoE') Probability of exceeding
        span#pETall
          span#pET0
            input.pageformcopy(name='probExcThresh0', type='text', value='0', size='3', maxlength='3')
            | ˚C, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET1
            input.pageformcopy(name='probExcThresh1', type='text', value='0', size='3', maxlength='3')
            | ˚C, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET2
            input.pageformcopy(name='probExcThresh2', type='text', value='0', size='3', maxlength='3')
            | ˚C, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET3
            input.pageformcopy(name='probExcThresh3', type='text', value='100', size='4', maxlength='4')
            | ˚C.days, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET4
            input.pageformcopy(name='probExcThresh4', type='text', value='30', size='3', maxlength='3')
            | days, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET5
            input.pageformcopy(name='probExcThresh5', type='text', value='30', size='3', maxlength='3')
            | days, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
          span#pET6
            input.pageformcopy(name='probExcThresh6', type='text', value='100', size='4', maxlength='4')
            | ˚C.days, as
            select.pageformcopy(name='poeunits')
              option(value='/unitless') fraction
              option(value='/percent') %
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='<<<water_sheds>>>') Water Sheds
          
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option(class='iridl:values region@value label')
      fieldset#wd.navitem
        legend Hot/Cold Day definition
        | Temperature above/below
        input.pageformcopy(name='hotThreshold', type='text', value='0.', size='3', maxlength='3')
        | ˚C
      fieldset.navitem(style='float: right;')
        legend Local Plots Range
        input.pageformcopy(name='plotrange1', type='text', size='5', maxlength='5')
        |  to
        input.pageformcopy(name='plotrange2', type='text', size='5', maxlength='5')
        span#pET0  ˚C
        span#pET1  ˚C
        span#pET2  ˚C
        span#pET6  ˚C.days
        span#pET3  ˚C.days
        span#pET4  days
        span#pET5  days
        span#pET6  ˚C.days
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
      fieldset.regionwithinbbox.dlimage
        a.dlimgts(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/%7Bnip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(MaxTemp)/eq/%7Bnip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(MeanTemp)/eq/%7Bnip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(CDD)/eq/%7B4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name/(Chilling%20Degree%20Days)/def%7Dif//seasonalStat/get_parameter/(GDD)/eq/%7B4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name/(Growing%20Degree%20Days)/def%7Dif//seasonalStat/get_parameter/(NumCD)/eq/%7B4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name/(Cold%20Days)/def%7Dif//seasonalStat/get_parameter/(NumHD)/eq/%7B4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name/(Hot%20Days)/def%7Dif/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//fullname%5Blong_name/(%20in)/append/units/cvntos%5Ddef/DATA/AUTO/AUTO/RANGE/T/exch/table:/2/:table/')
        img.dlimgloc(style='display: inline-block; float: left;', src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features_hydro>>>/X/Y/<<<littlegreymap_hydro>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            |  
            span.bold(class='iridl:long_name')
        #notgridbox.valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<water_sheds>>>)/geoobject/(<<<regiondef>>>)//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near
            |  
            span.bold(class='iridl:value')
        br
        br

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/{nip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MaxTemp)/eq/{nip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MeanTemp)/eq/{nip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(CDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Chilling%20Degree%20Days)def}if//seasonalStat/get_parameter/(GDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Growing%20Degree%20Days)def}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name(Cold%20Days)def}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name(Hot%20Days)def}if/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]weighted-average/a-/-a/T/fig-/medium/red/line/-fig//antialias/true/psdef/+.gif')
        br
        br

        img.dlimgts.poeg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/{nip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MaxTemp)/eq/{nip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MeanTemp)/eq/{nip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(CDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Chilling%20Degree%20Days)def}if//seasonalStat/get_parameter/(GDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Growing%20Degree%20Days)def}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name(Cold%20Days)def}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name(Hot%20Days)def}if/(<<<regiondef>>>)//region/parameter/geoobject/[X/Y]weighted-average/[T]0.0/99/{dup/1.0/100.0/div/add}repeat/0/replacebypercentile/percentile/-1/mul/1/add/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/{10.0/mul//units/(years out of 10)/def}if/use_as_grid/percentile/last/first/RANGE/a-/-a/percentile//long_name/(probability of exceeding)/def/fig-/medium/red/profile/-fig//antialias/true/psdef/+.gif')

      fieldset.dlimage.withMap
        a.dlimg(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/%7Bnip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(MaxTemp)/eq/%7Bnip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(MeanTemp)/eq/%7Bnip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/(CDD)/eq/%7B4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name/(Chilling%20Degree%20Days)/def%7Dif//seasonalStat/get_parameter/(GDD)/eq/%7B4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name/(Growing%20Degree%20Days)/def%7Dif//seasonalStat/get_parameter/(NumCD)/eq/%7B4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name/(Cold%20Days)/def%7Dif//seasonalStat/get_parameter/(NumHD)/eq/%7B4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name/(Hot%20Days)/def%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/0/maskle/Infinity/maskge/rainbowcolorscale/DATA/AUTO/AUTO/RANGE%7Dif//yearlyStat/get_parameter/(CoV)/eq/%7B/dup/%5BT%5Drmsaover/exch/%5BT%5Daverage/div//percent/unitconvert//scale_symmetric/false/def/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name//seasonalStat/get_parameter/(MinTemp)/eq/%7B/(0)//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/(MaxTemp)/eq/%7B/(0)//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/(MeanTemp)/eq/%7B/(0.)//probExcThresh0/parameter/interp%7Dif//seasonalStat/get_parameter/(CDD)/eq/%7B/(100)//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/(GDD)/eq/%7B/(100)//probExcThresh6/parameter/interp%7Dif//seasonalStat/get_parameter/(NumCD)/eq/%7B/(30)//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/(NumHD)/eq/%7B/(30)//probExcThresh5/parameter/interp%7Dif/3/-1/roll/exch/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20our%20of%2010)/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE%7Dif//name//clim_var/def/mark/exch/Y/exch/%5BX%5D/table:/mark/:table/')
        a(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/{nip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MaxTemp)/eq/{nip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MeanTemp)/eq/{nip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(CDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Chilling%20Degree%20Days)def}if//seasonalStat/get_parameter/(GDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Growing%20Degree%20Days)def}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name(Cold%20Days)def}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name(Hot%20Days)def}if/(Mean)//yearlyStat/parameter/(Mean)/eq/{[T]average/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(StdDev)/eq/{[T]/stddev/0/maskle/Infinity/maskge/rainbowcolorscale/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(CoV)/eq/{/dup/[T]rmsaover/exch/[T]average/div//percent/unitconvert//scale_symmetric/false/def/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap}if//yearlyStat/get_parameter/(PoE)/eq/{long_name//seasonalStat/get_parameter/(MinTemp)/eq/{/(0)//probExcThresh1/parameter/interp}if//seasonalStat/get_parameter/(MaxTemp)/eq/{/(0)//probExcThresh2/parameter/interp}if//seasonalStat/get_parameter/(MeanTemp)/eq/{/(0.)//probExcThresh0/parameter/interp}if//seasonalStat/get_parameter/(CDD)/eq/{/(100)//probExcThresh3/parameter/interp}if//seasonalStat/get_parameter/(GDD)/eq/{/(100)//probExcThresh6/parameter/interp}if//seasonalStat/get_parameter/(NumCD)/eq/{/(30)//probExcThresh4/parameter/interp}if//seasonalStat/get_parameter/(NumHD)/eq/{/(30)//probExcThresh5/parameter/interp}if/3/-1/roll/exch/flaggt/[T]average/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/{10./mul//units/(years our of 10)/def/DATA/0/10}{DATA/0/100}ifelse/RANGE}if//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/||/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/( for )/append/lpar/append//seasonStart/get_parameter/append/( )/append//DayStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/( )/append//DayEnd/get_parameter/append/rpar/append/psdef//layers[//clim_var//<<<layers_hydro>>>/]psdef/')

        img.dlimg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/{nip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MaxTemp)/eq/{nip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MeanTemp)/eq/{nip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(CDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Chilling%20Degree%20Days)def}if//seasonalStat/get_parameter/(GDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Growing%20Degree%20Days)def}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name(Cold%20Days)def}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name(Hot%20Days)def}if/(Mean)//yearlyStat/parameter/(Mean)/eq/{[T]average/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(StdDev)/eq/{[T]/stddev/0/maskle/Infinity/maskge/rainbowcolorscale/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(CoV)/eq/{/dup/[T]rmsaover/exch/[T]average/div//percent/unitconvert//scale_symmetric/false/def/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap}if//yearlyStat/get_parameter/(PoE)/eq/{long_name//seasonalStat/get_parameter/(MinTemp)/eq/{/(0)//probExcThresh1/parameter/interp}if//seasonalStat/get_parameter/(MaxTemp)/eq/{/(0)//probExcThresh2/parameter/interp}if//seasonalStat/get_parameter/(MeanTemp)/eq/{/(0.)//probExcThresh0/parameter/interp}if//seasonalStat/get_parameter/(CDD)/eq/{/(100)//probExcThresh3/parameter/interp}if//seasonalStat/get_parameter/(GDD)/eq/{/(100)//probExcThresh6/parameter/interp}if//seasonalStat/get_parameter/(NumCD)/eq/{/(30)//probExcThresh4/parameter/interp}if//seasonalStat/get_parameter/(NumHD)/eq/{/(30)//probExcThresh5/parameter/interp}if/3/-1/roll/exch/flaggt/[T]average/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/{10./mul//units/(years our of 10)/def/DATA/0/10}{DATA/0/100}ifelse/RANGE}if//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/||/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/( for )/append/lpar/append//seasonStart/get_parameter/append/( )/append//DayStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/( )/append//DayEnd/get_parameter/append/rpar/append/psdef//layers[//clim_var//<<<layers_hydro>>>/]psdef/+.gif')

        span#auxvar
          img.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Temperature/.daily/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGEEDGES/(1)//DayStart/parameter/( )/append/(Jul)//seasonStart/parameter/append/( - )/append/(30)//DayEnd/parameter/append/( )/append/(Sep)//seasonEnd/parameter/append/(0.)//hotThreshold/parameter/interp/(0.5)//minFrac/parameter/interp/(MeanTemp)//seasonalStat/parameter/(MinTemp)/eq/{nip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MaxTemp)/eq/{nip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(MeanTemp)/eq/{nip/3/-1/roll/.tmean/T/4/-2/roll/seasonalAverage}if//seasonalStat/get_parameter/(CDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Chilling%20Degree%20Days)def}if//seasonalStat/get_parameter/(GDD)/eq/{4/-1/roll/.tmean/3/-1/roll/sub/0/maskle/abs/3/-2/roll/flexseastotZeroFill//scale_symmetric/false/def//long_name(Growing%20Degree%20Days)def}if//seasonalStat/get_parameter/(NumCD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqLT//scale_symmetric/false/def//long_name(Cold%20Days)def}if//seasonalStat/get_parameter/(NumHD)/eq/{4/-1/roll/.tmean/4/-3/roll/flexseasonalfreqGT//scale_symmetric/false/def//long_name(Hot%20Days)def}if/(Mean)//yearlyStat/parameter/(Mean)/eq/{[T]average/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(StdDev)/eq/{[T]/stddev/0/maskle/Infinity/maskge/rainbowcolorscale/DATA/AUTO/AUTO/RANGE}if//yearlyStat/get_parameter/(CoV)/eq/{/dup/[T]rmsaover/exch/[T]average/div//percent/unitconvert//scale_symmetric/false/def/startcolormap/DATA/0/400/RANGE/transparent/black/navy/0/VALUE/navy/navy/10/bandmax/blue/blue/25/bandmax/DeepSkyBlue/DeepSkyBlue/50/bandmax/aquamarine/aquamarine/75/bandmax/PaleGreen/PaleGreen/90/bandmax/moccasin/moccasin/120/bandmax/yellow/yellow/150/bandmax/DarkOrange/DarkOrange/200/bandmax/red/red/250/bandmax/DarkRed/DarkRed/300/bandmax/brown/brown/400/bandmax/brown/endcolormap}if//yearlyStat/get_parameter/(PoE)/eq/{long_name//seasonalStat/get_parameter/(MinTemp)/eq/{/(0)//probExcThresh1/parameter/interp}if//seasonalStat/get_parameter/(MaxTemp)/eq/{/(0)//probExcThresh2/parameter/interp}if//seasonalStat/get_parameter/(MeanTemp)/eq/{/(0.)//probExcThresh0/parameter/interp}if//seasonalStat/get_parameter/(CDD)/eq/{/(100)//probExcThresh3/parameter/interp}if//seasonalStat/get_parameter/(GDD)/eq/{/(100)//probExcThresh6/parameter/interp}if//seasonalStat/get_parameter/(NumCD)/eq/{/(30)//probExcThresh4/parameter/interp}if//seasonalStat/get_parameter/(NumHD)/eq/{/(30)//probExcThresh5/parameter/interp}if/3/-1/roll/exch/flaggt/[T]average/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/{10./mul//units/(years our of 10)/def/DATA/0/10}{DATA/0/100}ifelse/RANGE}if//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/||/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/( for )/append/lpar/append//seasonStart/get_parameter/append/( )/append//DayStart/get_parameter/append/( - )/append//seasonEnd/get_parameter/append/( )/append//DayEnd/get_parameter/append/rpar/append/psdef//layers[//clim_var//<<<layers_hydro>>>/]psdef/+.auxfig/+.gif')

        span#auxtopo
          img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Daily Temperature Analysis
        p(property='term:description')
          | The Maproom explores historical daily temperature by calculating simple seasonal statistics.
        p
          | Many options can be specified to produce yearly time series of a chosen seasonal diagnostic of the daily temperature data. The user can then choose to map the mean, standard deviation, coefficient of variation or probability of exceeding a chosen threshold, over years; clicking on the map will then produce a local yearly time series of the chosen diagnostic.
        p(align='left')
          b Years and Season
          | :
          |             Specify the range of years over which to perform the analysis and choose the start and end dates of the season , over which the diagnostics are to be performed.
          br
          b Hot/Cold Day Definitions
          | :
          |             These define the temperature in Celsius (non inclusive) above which a day is considered to be a hot day (as opposed to cold). Note that hot/cold days are computed respectively with maximum/minimum temperatures.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Mean Temperature
          | : mean temperature (in Celsius) over the season.
          br
          b Minumum Temperature
          | : minimum temperature (in Celsius) over the season.
          br
          b Maximum Temperature
          | : maximum temperature (in Celsius) over the season.
          br
          b Number of Hot Days
          | : the number of hot days (as defined above) during the season.
          br
          b Number of Cold Days
          | : the number of cold days (as defined above) during the season.
          br
          b Chilling Degree Days
          | : chilling degree days are summations of negative differences between the mean daily temperature and user-defined reference base temperature during the season. They are comparable to the more familiar heating degree day definitions utilized by energy-sector.
          br
          b Growing Degree Days
          | : growing degree days are summations of positive differences between the mean daily temperature and user-defined reference base temperature during the season. They are comparable to the more familiar cooling degree day definitions utilized by energy-sector.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation, the coefficient of variation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | : The analysis can performed and map at each <<<gridres>>>&ring; resolution grid point. Additionally it is possible to average the results of the analysis over the <<<gridres>>>&ring; grid points falling within administrative boundaries for the time series graph.
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall  on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km) from National Meteorology Agency. The time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations in NMA&rsquo;s archive with satellite rainfall estimates.
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions
        p
          | The
          b Plots Range
          |  controls define the range of the vertical axis of the time series and the horizontal axis of the probability of exceeding graph. Blanking the boxes will set the range to the extrema of graph in view.
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.

    br
    .optionsBar
      fieldset#share.navitem
        legend Share
