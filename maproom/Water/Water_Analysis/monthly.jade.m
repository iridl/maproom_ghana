=== title = "Monthly Climate Analysis"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  
  head
    meta(name='viewport', content='width=device-width, initial-scale=1.0;')
    title <<<title>>>
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link.share(rel='canonical', href='monthly.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', href='/maproom/navmenu.json')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Analysis')
    meta(xml:lang='', property='maproom:Sort_Id', content='a3')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')
    
    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly/.c9120//var/get_parameter/interp/T/(Oct)/VALUE//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/1/index/geometrycovers/-1/mul/1/add/flagfiltergeom//name//regMask/def/SOURCES/.Features/.Political/.World/.Countries/.the_geom//name//cntryMask/def/objectid/<<<surrounding_countries>>>/VALUES/X/Y/fig-/colors/<<<graph_hydro>>>/grey/strokefill/black/strokefill/-fig//layers%5B//clim_var//<<<layers_hydro>>>//cntryMask%5Dpsdef//antialias/true/psdef//plotborder/0/psdef//plotaxislength/220/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='../../../localconfig/ui.js')

    style.
      .dlauximg.bis {
      padding-left:30px;
      width: 89%;
      }
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="clim_var"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="clim_var"] #auxtopo {
      display: none;
      }

  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.share.dlimgloc.dlimglocclick(name='bbox', type='hidden')
      input.dlimg.dlauximg.share(name='T', type='hidden', value='Jan')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.carry.pickarea.admin(name='resolution', type='hidden', value='<<<gridres>>>')
      input.carry.share.ver2(name='YearStart', type='hidden', value='<<<yearstadef>>>')
      input.carry.share.ver2(name='YearEnd', type='hidden', value='<<<yearenddef>>>')
      input.carry.share.ver2(name='seasonStart', type='hidden', value='Jul')
      input.carry.share.ver2(name='seasonEnd', type='hidden', value='Sep')
      input.carry.share.dlimg.dlauximg.dlimgts(name='var', type='hidden', value='.precip')
      input.dlimg.share.bodyAttribute(name='layers', value='clim_var', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Water_Sheds', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='regMask', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='cntryMask', checked='checked', type='checkbox')
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Water/') Climate and Water
      
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
          span(property='term:label') Analysis
      
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      
      fieldset.navitem
        legend Variable
        select.pageformcopy(name='var')
          option(value='.precip') Rainfall
          option(value='.tmax') Maximum Temperature
          option(value='.tmin') Minimum Temperature
          option(value='.tmean') Mean Temperature
      
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='<<<water_sheds>>>') Water Sheds
          
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(ds)/rsearch/%7Bpop/pop/pop/geoobject/dup/parent/.dataset/.label/gid/iridl%3AgeoId/(%3Ads)/rsearch/%7Bpop/pop%7Dif/(%3Agid%40%25d%3Ads)/append/sprintf//name//region/def/use_as_grid%7D%7Binterp/c%3A/exch/%3Ac/grid%3A//name//region/def/values%3A/(<<<regiondef>>>)//region/parameter/%3Avalues/%3Agrid/addGRID//name//label/def%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option.label(class='iridl:values region@value label')
      
      fieldset.navitem
        legend Yearly Seasonal Anomalies
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
      
      fieldset.regionwithinbbox.dlimage.bis(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features_hydro>>>/X/Y/<<<littlegreymap_hydro>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')

        // <b>Monthly Precipitation Climatology</b>
        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/T/first/(<<<yearenddef>>>)/RANGE/T/12/splitstreamgrid/dup/%5BT2%5Daverage/exch/%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a%3A/percentile/0.05/VALUE/percentile/removeGRID//fullname/(5th%20%25-ile)/def/%3Aa%3A/percentile/0.5/VALUE/percentile/removeGRID//fullname/(50th%20%25-ile)/def/%3Aa%3A/percentile/0.95/VALUE/percentile/removeGRID//fullname/(95th%20%25-ile)/def/%3Aa/2/index//var/get_parameter/(.precip)/eq/%7B0.0/mul%7D%7B%5BT%5Dminover%7Difelse/dup/6/-1/roll/exch/6/-2/roll/5/index/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//var/get_parameter/(.precip)/eq/%7B//framelabel/(Monthly%20Rainfall%20Climatology)/psdef%7Dif//var/get_parameter/(.tmax)/eq/%7B//framelabel/(Monthly%20Maximum%20Temperature%20Climatology)/psdef%7Dif//var/get_parameter/(.tmin)/eq/%7B//framelabel/(Monthly%20Minimum%20Temperature%20Climatology)/psdef%7Dif//var/get_parameter/(.tmean)/eq/%7B//framelabel/(Monthly%20Mean%20Temperature%20Climatology)/psdef%7Dif//plotborderbottom/40/psdef//antialias/true/def/+.gif')
        br
        br

        img.dlimgts.ver2(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly//var/get_parameter/interp/(<<<regiondef>>>)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/(Jul)//seasonStart/parameter/(-)/append/(Sep)//seasonEnd/parameter/append/(%20)/append/(<<<yearstadef>>>)//YearStart/parameter/append/(-)/append/(<<<yearenddef>>>)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.precip)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1.0/add%7D%7B13.0/add%7Difelse/1.0/mul/mul%7Dif/dup/%5BT%5Daverage/sub//var/get_parameter/(.precip)/eq/%7Bprcp_anomaly_max1000_colors2%7D%7Bsstacolorscale%7Difelse//name//ClimVar/def/DATA/AUTO/AUTO/RANGE/a%3A//var/get_parameter/(.precip)/eq/%7B//long_name/(rainfall)/def%7Dif//var/get_parameter/(.tmax)/eq/%7B//long_name/(maximum%20temperature)/def%7Dif//var/get_parameter/(.tmin)/eq/%7B//long_name/(minimum%20temperature)/def%7Dif//var/get_parameter/(.tmean)/eq/%7B//long_name/(mean%20temperature)/def%7Dif/%3Aa%3A/%3Aa/T/fig-/colorbars2/-fig//plotaxislength/400/psdef//var/get_parameter/(.precip)/eq/%7B//framelabel/(Yearly%20Seasonal%20Rainfall%20Anomalies)/psdef%7Dif//var/get_parameter/(.tmax)/eq/%7B//framelabel/(Yearly%20Seasonal%20Max%20Temperature%20Anomalies)/psdef%7Dif//var/get_parameter/(.tmin)/eq/%7B//framelabel/(Yearly%20Seasonal%20Min%20Temperature%20Anomalies)/psdef%7Dif//var/get_parameter/(.tmean)/eq/%7B//framelabel/(Yearly%20Seasonal%20Mean%20Temperature%20Anomalies)/psdef%7Dif//plotborderbottom/40/psdef/+.gif')

      fieldset.dlimage.withMap
        link(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/1/index/geometrycovers/-1/mul/1/add/flagfiltergeom//name//regMask/def/SOURCES/.Features/.Political/.World/.Countries/.the_geom//name//cntryMask/def/objectid/<<<surrounding_countries>>>/VALUES/X/Y/fig-/colors/<<<graph_hydro>>>/grey/strokefill/black/strokefill/-fig//layers%5B//clim_var//<<<layers_hydro>>>//cntryMask%5Dpsdef/')
        img.dlimg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/1/index/geometrycovers/-1/mul/1/add/flagfiltergeom//name//regMask/def/SOURCES/.Features/.Political/.World/.Countries/.the_geom//name//cntryMask/def/objectid/<<<surrounding_countries>>>/VALUES/X/Y/fig-/colors/<<<graph_hydro>>>/grey/strokefill/black/strokefill/-fig//layers%5B//clim_var//<<<layers_hydro>>>//cntryMask%5Dpsdef/T/0.5/plotvalue+.gif', border='0', alt='image')
        br
        span#auxvar
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/(.precip)//var/parameter/(.precip)/eq/{.Rainfall}{.Temperature}ifelse/.monthly/.c9120//var/get_parameter/interp//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/SOURCES/.Features/.Political/.Ghana/.Regions/.the_geom/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/1/index/geometrycovers/-1/mul/1/add/flagfiltergeom//name//regMask/def/SOURCES/.Features/.Political/.World/.Countries/.the_geom//name//cntryMask/def/objectid/<<<surrounding_countries>>>/VALUES/X/Y/fig-/colors/<<<graph_hydro>>>/grey/strokefill/black/strokefill/-fig//layers%5B//clim_var//<<<layers_hydro>>>//cntryMask%5Dpsdef/T/0.5/plotvalue+.auxfig/+.gif')
        span#auxtopo
          img.dlauximg.bis(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/++.auxfig+//plotaxislength+432+psdef//plotborder+72+psdef//fntsze/20/psdef/+.gif')
      
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') <<<title>>>
        p(property='term:description')
          | Rainfall time series (<<<yearstadef>>>-<<<yearenddef>>>) and temperature time series (<<<yearstadef>>>-<<<yearenddef>>>) reconstructed from station observations, remote sensing and other proxies. This interface allows users to view rainfall, maximum, minimum and mean temperature monthly climatologies and anomalies.
      
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall and temperature data over land areas on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4 km of resolution).
          | The rainfall time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations with satellite rainfall estimates.
          | Minimum and maximum temperature time series (<<<yearstadef>>> to <<<yearenddef>>>) were generated by combining quality-controlled station observations with downscaled reanalysis product.
      
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis
      
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p.p1
          | Contact
          a(href='mailto:<<<helpemail>>>?subject= <<<title>>> - <<<country_name>>>') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.
          br

    .optionsBar
      fieldset#share.navitem
        legend Share
