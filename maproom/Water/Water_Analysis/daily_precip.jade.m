doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    title Daily Precipitation Analysis
    
    link(rel='stylesheet', type='text/css', href='/maproom/<<<css_stem>>>.css')
    link.share(rel='canonical', href='daily_precip.html')
    link(rel='home', href='<<<homeURL>>>')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='shortcut icon', href='/icons/<<<an_icon>>>', sizes='any', type='<<<an_icon_type>>>')
    meta(xml:lang='', property='maproom:Entry_Id', content='Climatology_Daily_Precip_Analysis')
    meta(xml:lang='', property='maproom:Sort_Id', content='0a1')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')

    link(rel='term:icon', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/11565.5/VALUE/cmorph_dekad_colors/<<<features_hydro>>>/X/Y/fig-/colors/<<<graph_hydro>>>/-fig//plotaxislength/220/psdef//antialias/true/psdef//plotborder/0/psdef/+.gif')

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/maproom/<<<css_stem>>>.js')
    
    style.
      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="clim_var"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="clim_var"] #auxtopo {
      display: none;
      }
      .seasonalStat #wd {
      display: none;
      }
      .seasonalStatTotRain #wd {
      display: none;
      }
      .seasonalStat #ds {
      display: none;
      }
      .seasonalStatTotRain #ds {
      display: none;
      }
      .seasonalStatNumWD #ds {
      display: none;
      }
      .seasonalStatNumDD #ds {
      display: none;
      }
      .seasonalStatRainInt #ds {
      display: none;
      }
      .seasonalStatTotRain #pET2 {
      display: none;
      }
      .seasonalStatTotRain #pET3 {
      display: none;
      }
      .seasonalStatTotRain #pET4 {
      display: none;
      }
      .seasonalStatTotRain #pET5 {
      display: none;
      }
      .seasonalStatTotRain #pET6 {
      display: none;
      }
      .seasonalStatNumWD #pET1 {
      display: none;
      }
      .seasonalStatNumWD #pET3 {
      display: none;
      }
      .seasonalStatNumWD #pET4 {
      display: none;
      }
      .seasonalStatNumWD #pET5 {
      display: none;
      }
      .seasonalStatNumWD #pET6 {
      display: none;
      }
      .seasonalStatNumDD #pET1 {
      display: none;
      }
      .seasonalStatNumDD #pET2 {
      display: none;
      }
      .seasonalStatNumDD #pET4 {
      display: none;
      }
      .seasonalStatNumDD #pET5 {
      display: none;
      }
      .seasonalStatNumDD #pET6 {
      display: none;
      }
      .seasonalStatRainInt #pET1 {
      display: none;
      }
      .seasonalStatRainInt #pET2 {
      display: none;
      }
      .seasonalStatRainInt #pET3 {
      display: none;
      }
      .seasonalStatRainInt #pET5 {
      display: none;
      }
      .seasonalStatRainInt #pET6 {
      display: none;
      }
      .seasonalStatNumDS #pET1 {
      display: none;
      }
      .seasonalStatNumDS #pET2 {
      display: none;
      }
      .seasonalStatNumDS #pET3 {
      display: none;
      }
      .seasonalStatNumDS #pET4 {
      display: none;
      }
      .seasonalStatNumDS #pET6 {
      display: none;
      }
      .seasonalStatNumWS #pET1 {
      display: none;
      }
      .seasonalStatNumWS #pET2 {
      display: none;
      }
      .seasonalStatNumWS #pET3 {
      display: none;
      }
      .seasonalStatNumWS #pET4 {
      display: none;
      }
      .seasonalStatNumWS #pET5 {
      display: none;
      }
      .seasonalStat #pET2 {
      display: none;
      }
      .seasonalStat #pET3 {
      display: none;
      }
      .seasonalStat #pET4 {
      display: none;
      }
      .seasonalStat #pET5 {
      display: none;
      }
      .seasonalStat #pET6 {
      display: none;
      }
      .seasonalStatPerDA #yearlyStat {
      display: none;
      }

      .yearlyStatMean #pET1 {
      display: none;
      }
      .yearlyStatStdDev #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }

      .yearlyStatMean #pET2 {
      display: none;
      }
      .yearlyStatStdDev #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }

      .yearlyStatMean #pET3 {
      display: none;
      }
      .yearlyStatStdDev #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }
      .yearlyStatMean #pET4 {
      display: none;
      }
      .yearlyStatStdDev #pET4 {
      display: none;
      }
      .yearlyStat #pET4 {
      display: none;
      }
      .yearlyStatMean #pET5 {
      display: none;
      }
      .yearlyStatStdDev #pET5 {
      display: none;
      }
      .yearlyStat #pET5 {
      display: none;
      }

      .yearlyStatMean #pET6 {
      display: none;
      }
      .yearlyStatStdDev #pET6 {
      display: none;
      }
      .yearlyStat #pET6 {
      display: none;
      }

      .seasonalStatTotRain #pr2 {
      display: none;
      }
      .seasonalStatTotRain #pr3 {
      display: none;
      }
      .seasonalStatTotRain #pr4 {
      display: none;
      }
      .seasonalStatTotRain #pr5 {
      display: none;
      }
      .seasonalStatTotRain #pr6 {
      display: none;
      }
      .seasonalStatNumWD #pr1 {
      display: none;
      }
      .seasonalStatNumWD #pr3 {
      display: none;
      }
      .seasonalStatNumWD #pr4 {
      display: none;
      }
      .seasonalStatNumWD #pr5 {
      display: none;
      }
      .seasonalStatNumWD #pr6 {
      display: none;
      }
      .seasonalStatNumDD #pr1 {
      display: none;
      }
      .seasonalStatNumDD #pr2 {
      display: none;
      }
      .seasonalStatNumDD #pr4 {
      display: none;
      }
      .seasonalStatNumDD #pr5 {
      display: none;
      }
      .seasonalStatNumDD #pr6 {
      display: none;
      }
      .seasonalStatRainInt #pr1 {
      display: none;
      }
      .seasonalStatRainInt #pr2 {
      display: none;
      }
      .seasonalStatRainInt #pr3 {
      display: none;
      }
      .seasonalStatRainInt #pr5 {
      display: none;
      }
      .seasonalStatRainInt #pr6 {
      display: none;
      }
      .seasonalStatNumDS #pr1 {
      display: none;
      }
      .seasonalStatNumDS #pr2 {
      display: none;
      }
      .seasonalStatNumDS #pr3 {
      display: none;
      }
      .seasonalStatNumDS #pr4 {
      display: none;
      }
      .seasonalStatNumDS #pr6 {
      display: none;
      }
      .seasonalStatNumWS #pr1 {
      display: none;
      }
      .seasonalStatNumWS #pr2 {
      display: none;
      }
      .seasonalStatNumWS #pr3 {
      display: none;
      }
      .seasonalStatNumWS #pr4 {
      display: none;
      }
      .seasonalStatNumWS #pr5 {
      display: none;
      }
      .seasonalStat #pr2 {
      display: none;
      }
      .seasonalStat #pr3 {
      display: none;
      }
      .seasonalStat #pr4 {
      display: none;
      }
      .seasonalStat #pr5 {
      display: none;
      }
      .seasonalStat #pr6 {
      display: none;
      }
      
      body[resolution='<<<water_sheds>>>'] #notgridbox {
      display: none;
      }

  body(xml:lang='en')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.carry.dlimg.dlauximg.share.dlimgloc.dlimglocclick.admin(name='bbox', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.pickarea.share.carry.admin.bodyAttribute(name='resolution', type='hidden', data-default='<<<gridres>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='YearStart', type='hidden', data-default='<<<yearstadef>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='YearEnd', type='hidden', data-default='<<<yearenddef>>>')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jul')
      input.carry.dlimg.dlauximg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Sep')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='01')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='30')
      input.dlimg.dlauximg.share.dlimgts(name='wetThreshold', type='hidden', data-default='1.')
      input.dlimg.dlauximg.share.dlimgts(name='spellThreshold', type='hidden', data-default='5.')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='TotRain')
      input.dlimg.dlauximg.share.carry.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='Mean')
      input.dlimg.share.pET(name='probExcThresh1', type='hidden', data-default='40.')
      input.dlimg.share.pET(name='probExcThresh2', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh3', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh4', type='hidden', data-default='10.')
      input.dlimg.share.pET(name='probExcThresh5', type='hidden', data-default='3.')
      input.dlimg.share.pET(name='probExcThresh6', type='hidden', data-default='3.')
      input.dlimg.share.pET(name='probExcThresh7', type='hidden', data-default='20.')
      input.dlimg.dlauximg.carry.poeg.share.pET(name='poeunits', type='hidden', data-default='/percent')
      input.dlimg.share.bodyAttribute(name='layers', data-default='clim_var', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='bath', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', data-default='Water_Sheds', checked='checked', type='checkbox')
      input.dlimgts.share(name='plotrange1', type='hidden')
      input.dlimgts.share(name='plotrange2', type='hidden')
    // *********************
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Water/') Climate and Water
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Water_Analysis')
          span(property='term:label') Analysis
      fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/<<<regions_stem>>>.json')
        select.RegionMenu(name='bbox')
          option(value='') <<<country_name>>>
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Time Period (<<<yearstadef>>> to <<<yearenddef>>>)
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='01', size='2', maxlength='2')
        |          to
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='30', size='2', maxlength='2')
        |         -
        input.pageformcopy(name='YearStart', type='text', value='<<<yearstadef>>>', size='4', maxlength='4')
        |  to
        input.pageformcopy(name='YearEnd', type='text', value='<<<yearenddef>>>', size='4', maxlength='4')
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='TotRain') Total Rainfall
          option(value='NumWD') Number of Wet Days
          option(value='NumDD') Number of Dry Days
          option(value='RainInt') Rainfall Intensity
          option(value='NumDS') Number of Dry Spells
          option(value='NumWS') Number of Wet Spells
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='Mean') Mean
          option(value='StdDev') Standard deviation
          option(value='PoE') Probability of exceeding
        span#pET1
          input.pageformcopy(name='probExcThresh1', type='text', value='40', size='3', maxlength='6')
          | mm, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
        span#pET2
          input.pageformcopy(name='probExcThresh2', type='text', value='30', size='3', maxlength='3')
          | days, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
        span#pET3
          input.pageformcopy(name='probExcThresh3', type='text', value='30', size='3', maxlength='3')
          | days, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
        span#pET4
          input.pageformcopy(name='probExcThresh4', type='text', value='10', size='3', maxlength='3')
          | mm/day, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
        span#pET5
          input.pageformcopy(name='probExcThresh5', type='text', value='3', size='3', maxlength='3')
          | spells, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
        span#pET6
          input.pageformcopy(name='probExcThresh6', type='text', value='3', size='3', maxlength='3')
          | spells, as
          select.pageformcopy(name='poeunits')
            option(value='/unitless') fraction
            option(value='/percent') %
      
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='<<<gridres>>>') gridpoint
          option(value='<<<water_sheds>>>') Water Sheds
          
        link.admin(rel='iridl:hasJSON', href='/expert/(<<<gridres>>>)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(<<<regiondef>>>)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json')
        select.pageformcopy(name='region')
          optgroup.template(label='Label')
            option(class='iridl:values region@value label')
      fieldset#wd.navitem
        legend Wet/Dry Day definition
        | Rainfall amount above/below
        input.pageformcopy(name='wetThreshold', type='text', value='1.', size='5', maxlength='5')
        | mm/day
      fieldset#ds.navitem
        legend Wet/Dry Spell definition
        input.pageformcopy(name='spellThreshold', type='text', value='5', size='2', maxlength='2')
        | continuous wet/dry days
      fieldset.navitem(style='float: right;')
        legend Local Plots Range
        input.pageformcopy(name='plotrange1', type='text', size='5', maxlength='5')
        |  to
        input.pageformcopy(name='plotrange2', type='text', size='5', maxlength='5')
        span#pr1  mm
        span#pr2  days
        span#pr3  days
        span#pr4  mm/day
        span#pr5  spells
        span#pr6  spells

    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us
      fieldset.regionwithinbbox.dlimage
        a.dlimgts(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/(<<<regiondef>>>)//region/geoobject/%5BX/Y%5Dweighted-average/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/(5)//spellThreshold/parameter/interp/(1)//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/precip_colors%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days)/def/wetday_freq_colors%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days)/def/wetday_freq_colors%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity)/def/prcp_hrlyrate_colors%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells)/def/cmorph_dekad_colors%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells)/def/wetday_freq_colors%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif//fullname%5Blong_name/(%20in)/append/units/cvntos%5Ddef/DATA/AUTO/AUTO/RANGE/T/exch/table:/2/:table/')

        img.dlimgloc(style='display: inline-block; float: left;', src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A<<<bbW>>>%3A<<<bbS>>>%3A<<<bbE>>>%3A<<<bbN>>>%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(<<<regiondef>>>)/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/<<<features_hydro>>>/X/Y/<<<littlegreymap_hydro>>>//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<regiondef>>>)//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')
        #notgridbox.valid
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(<<<water_sheds>>>)/geoobject/(<<<regiondef>>>)//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near 
            span.bold(class='iridl:value')

        img.dlimgts(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/(<<<regiondef>>>)//region/parameter/geoobject/long_name/3/-2/roll/%5BX/Y%5Dweighted-average/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//long_name/(Total%20Rainfall)/def//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days)/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days)/def/wetday_freq_colors%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity)/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells)/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells)/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif//seasonalStat/get_parameter/0/3/getinterval/(Num)/eq/%7B0.5/add/toi4%7Dif//fullname/%5Blong_name/(%20in)/append/units/cvntos/%5D/def/DATA/0//plotrange1/parameter/AUTO//plotrange2/parameter/RANGE/dup/a-/-a/T/fig-/colorbars2/-fig//antialias/true/psdef//framelabel/%5B/8/-1/roll/(%20)/6/index/.long_name//seasonalStat/get_parameter/(NumWD)/eq/%7B/(%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/(%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/(%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/(%20-%20)//spellThreshold/get_parameter/s%3D%3D/(%20d%20below%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/(%20-%20)//spellThreshold/get_parameter/s%3D%3D/(%20or%20more%20continuous%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif/(%20)//DayStart/get_parameter/(%20)//seasonStart/get_parameter/(%20-%20)//DayEnd/get_parameter/(%20)//seasonEnd/get_parameter/%5D/concat/psdef//plotbordertop/40/psdef//plotborderbottom/80/psdef/+.gif')
        br
        br

        img.dlimgts.poeg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/(<<<regiondef>>>)//region/geoobject/long_name/3/-2/roll/%5BX/Y%5Dweighted-average/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//long_name/(Total%20Rainfall)/def//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days)/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days)/def/wetday_freq_colors%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity)/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells)/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells)/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%5BT%5D/0./99/%7B/dup/1./100./div/add/%7D/repeat/0/replacebypercentile/percentile/-1/mul/1/add/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20out%20of%2010)/def%7Dif/use_as_grid//fullname/%5Blong_name/(%20in)/append/units/cvntos/%5D/def/DATA/AUTO//plotrange1/parameter/AUTO//plotrange2/parameter/RANGE/percentile/last/first/RANGE/a-/-a/percentile//long_name/(probability%20of%20exceeding)/def/fig-/blue/medium/profile/-fig//antialias/true/psdef//framelabel/%5B/7/-1/roll/(%20)/6/index/.long_name//seasonalStat/get_parameter/(NumWD)/eq/%7B/(%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/(%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/(%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/(%20-%20)//spellThreshold/get_parameter/s%3D%3D/(%20d%20below%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/(%20-%20)//spellThreshold/get_parameter/s%3D%3D/(%20or%20more%20continuous%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/(%20mm)/%7Dif/(%20)//DayStart/get_parameter/(%20)//seasonStart/get_parameter/(%20-%20)//DayEnd/get_parameter/(%20)//seasonEnd/get_parameter/%5D/concat/psdef/+.gif')

      fieldset.dlimage.withMap
        a.dlimg(rel='iridl:hasTable', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20below%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20above%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//seasonalStat/get_parameter/(TotRain)/eq/%7B/40.//probExcThresh1/parameter/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/30.//probExcThresh3/parameter/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/10.//probExcThresh4/parameter/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/3.//probExcThresh5/parameter/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/3.//probExcThresh6/parameter/%7Dif/flaggt/%5BT%5Daverage//exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20our%20of%2010)/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE/%7Dif//name//clim_var/def/mark/exch/Y/exch/%5BX%5D/table:/mark/:table/')

        a(rel='iridl:hasFigure', href='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20below%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20above%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//seasonalStat/get_parameter/(TotRain)/eq/%7B/40.//probExcThresh1/parameter/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/30.//probExcThresh3/parameter/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/10.//probExcThresh4/parameter/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/3.//probExcThresh5/parameter/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/3.//probExcThresh6/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20our%20of%2010)/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20)/append//DayStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/(%20)/append//DayEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers_hydro>>>/%5Dpsdef/')

        img.dlimg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20below%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20above%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//seasonalStat/get_parameter/(TotRain)/eq/%7B/40.//probExcThresh1/parameter/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/30.//probExcThresh3/parameter/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/10.//probExcThresh4/parameter/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/3.//probExcThresh5/parameter/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/3.//probExcThresh6/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20our%20of%2010)/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20)/append//DayStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/(%20)/append//DayEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers_hydro>>>/%5Dpsdef/+.gif')

        span#auxvar
          img.dlauximg(rel='iridl:hasFigureImage', src='<<<ENACTSv>>>/.ALL/.Rainfall/.daily/.precip/T/(<<<yearstadef>>>)//YearStart/parameter/(<<<yearenddef>>>)//YearEnd/parameter/RANGE/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(30)//DayEnd/parameter/append/(%20)/append/(Sep)//seasonEnd/parameter/append/5//spellThreshold/parameter/1//wetThreshold/parameter/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//units//mm/def/precip_colors/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Number%20of%20Wet%20Days%20-%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B3/-1/roll/pop/flexseasonalfreqLT//long_name/(Number%20of%20Dry%20Days%20-%20days%20below%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall%20Intensity%20-%20average%20daily%20rainfall%20for%20days%20above%20)//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/prcp_hrlyrate_colors/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Number%20of%20Dry%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20below%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/cmorph_dekad_colors/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Number%20of%20Wet%20Spells%20-%20)//spellThreshold/get_parameter/s%3D%3D/append/(%20or%20more%20continuous%20days%20above%20)/append//wetThreshold/get_parameter/s%3D%3D/append/(%20mm)/append/def/wetday_freq_colors/%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/(Mean)//yearlyStat/parameter/(Mean)/eq/%7B%5BT%5Daverage/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(StdDev)/eq/%7B%5BT%5D/stddev/rainbowcolorscale/DATA/AUTO/AUTO/RANGE/%7Dif//yearlyStat/get_parameter/(PoE)/eq/%7Blong_name/exch//seasonalStat/get_parameter/(TotRain)/eq/%7B/40.//probExcThresh1/parameter/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B/30.//probExcThresh2/parameter/%7Dif//seasonalStat/get_parameter/(NumDD)/eq/%7B/30.//probExcThresh3/parameter/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/10.//probExcThresh4/parameter/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/3.//probExcThresh5/parameter/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/3.//probExcThresh6/parameter/%7Dif/flaggt/%5BT%5Daverage/exch//long_name/exch/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap/(/percent)//poeunits/parameter/interp/unitconvert//poeunits/get_parameter/(/unitless)/eq/%7B10./mul//units/(years%20our%20of%2010)/def/DATA/0/10%7D%7BDATA/0/100%7Difelse/RANGE/%7Dif//name//clim_var/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/1/index/<<<features_hydro>>>/X/Y/fig-/colors/colors/%7C%7C/colors/<<<graph_hydro>>>/-fig//antialias/true/psdef//framelabel//yearlyStat/get_parameter/(%20for%20)/append/lpar/append//seasonStart/get_parameter/append/(%20)/append//DayStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/(%20)/append//DayEnd/get_parameter/append/rpar/append/psdef//layers%5B//clim_var//<<<layers_hydro>>>/%5Dpsdef/+.auxfig/+.gif')

        span#auxtopo
          img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.WORLDBATH/.bath/X/<<<bbW>>>/<<<bbE>>>/RANGE/Y/<<<bbS>>>/<<<bbN>>>/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Daily Precipitation Analysis
        p(property='term:description')
          | The Maproom explores historical daily precipitation by calculating simple seasonal statistics.
        p
          | Many options can be specified to produce yearly time series of a chosen seasonal diagnostic of the daily precipitation data. The user can then choose to map the mean, standard deviation or probability of exceeding a chosen threshold, over years; clicking on the map will then produce a local yearly time series of the chosen diagnostic.
        p(align='left')
          b Years and Season
          | :
          |             Specify the range of years over which to perform the analysis and choose the start and end dates of the season , over which the diagnostics are to be performed.
          br
          b Wet/Dry Day/Spell Definitions
          | :
          |             These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Total Rainfall
          | : total cumulative precipitation (in mm) falling over the season.
          br
          b Number of Wet Days
          | : the number of wet days (as defined above) during the season.
          br
          b Number of Dry Days
          | : the number of dry days (as defined above) during the season.
          br
          b Rainfall Intensity
          | :
          |             the average daily precipitation over the season considering only wet days.
          br
          b Number of Wet (Dry) Spells
          | :
          |             the number of wet (dry) spells during the season according to the definitions in the Options section. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | :
          |             The analysis can performed and map at each <<<gridres>>>&ring; resolution grid point. Additionally it is possible to average the results of the analysis over the <<<gridres>>>&ring; grid points falling within administrative boundaries for the time series graph.
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          | Reconstructed rainfall  on a <<<gridres>>>&ring; x <<<gridres>>>&ring; lat/lon grid (about 4km) from National Meteorology Agency. The time series (<<<yearstadef>>> to <<<yearenddef>>>) were created by combining quality-controlled station observations in NMA&rsquo;s archive with satellite rainfall estimates.
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions
        p
          | The
          b Plots Range
          |  controls define the range of the vertical axis of the time series and the horizontal axis of the probability of exceeding graph. Blanking the boxes will set the range to the extrema of graph in view.
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p
          | Contact
          a(href='mailto:<<<helpemail>>>?subject=Climate Analysis') <<<helpemail>>>
          |  with any technical questions or problems with this Map Room.

    br
    .optionsBar
      fieldset#share.navitem
        legend Share
