require "functions"

Languages={{"en"},{"en"}}

RepoDir = os.getenv('PWD')
Host = "127.0.0.2"
Port = 80
ServerAdmin = "webmaster@localhost"
ServerName = "127.0.0.2"
ServerAlias = "127.0.0.2"
ProxyModule = true -- set to false if you would like to use rewrite rules
ProxyURL = "http://213.154.77.59"
MacSetup = false

if ProxyModule then
ProxyRules = [[
   ProxyPass /SOURCES ]]..ProxyURL..[[/SOURCES nocanon
   ProxyPass /expert ]]..ProxyURL..[[/expert nocanon
   ProxyPass /ds: ]]..ProxyURL..[[/ds: nocanon
   ProxyPass /home ]]..ProxyURL..[[/home nocanon
   ProxyPass /openrdf-sesame ]]..ProxyURL..[[/openrdf-sesame nocanon
]]
else
ProxyRules = [[
   RewriteRule ^/SOURCES/(.*) ]]..ProxyURL..[[/SOURCES/$1
   RewriteRule ^/expert/(.*) ]]..ProxyURL..[[/expert/$1
   RewriteRule ^/ds:/(.*) ]]..ProxyURL..[[/ds:/$1
   RewriteRule ^/home/(.*) ]]..ProxyURL..[[/home/$1
   RewriteRule ^/openrdf-sesame/(.*) ]]..ProxyURL..[[/openrdf-sesame/$1
]]
end


--[[ This starts the parameters for Maprooms ]]

an_icon = "GMet_Logo.jpg"
an_icon_type = "jpg"
ENACTSv = "/SOURCES/.GhanaMet/.ENACTS/.ENACTS_v3"

regiondef = "bb%3A0%3A6%3A0.0375%3A6.0375%3Abb"
gridres = "0.0375"
css_stem = "ghana"
homeURL = "http://www.meteo.gov.gh"
bbW = "-3.6375"
bbE = "1.65"
bbS = "4.345"
bbN = "11.625"
regions_stem = "ghanaregions"
country_name = "Ghana"
helpemail = "p.lamptey@meteo.gov.gh"
yearstadef = "1991"
yearenddef = "2020"
surrounding_countries = "78/94/95/98"
monit_startDay = "1 Jan"

features = "SOURCES/.Features/.Political/.Ghana/a:/.Districts/.the_geom/:a:/.Regions/.the_geom/:a"
graph = "grey/thin/stroke/black/thin/stroke"
littlegreymap = "fig-/lightgrey/mask/red/fill/red/smallpushpins/grey/stroke/black/stroke/blue/thin/rivers_gaz/-fig"
layers = "Districts//Regions"

features_hydro = "SOURCES/.Features/.Hydrological/.Water_Sheds/.the_geom"
graph_hydro = "black/stroke"
littlegreymap_hydro = "fig-/lightgrey/mask/red/fill/red/smallpushpins/black/stroke/blue/thin/rivers_gaz/-fig"
layers_hydro = "Water_Sheds"

--[[ Resolutions --]]
districts = "irids:SOURCES:Features:Political:Ghana:Districts:ds"
regions = "irids:SOURCES:Features:Political:Ghana:Regions:ds"
water_sheds = "irids:SOURCES:Features:Hydrological:Water_Sheds:ds"

--[[Water Climate Maprooms --]]
precip_climatology = ".c9120/.precip"
precip_clim_text ='1991-2020'
precip_spi = ".SPI-precip_1-dekad"

--[[ This starts the section of parameters for the Tutorials.--]]

--[[Parameters for Tutoria/Datasets/Location.--]]

--[[ Data by Source --]]
LocationText1 = "the Agence Nationale de l'Aviation Civile et de la Météorologie"
LocationText2 = "ANACIM"
LocationText3 = "ENACTS"
LocationText4 = "ENACTS version2 ALL monthly temperature"
LocationText5 = "Select the <i>ENACTS</i> link.<br />Select the <i>version2</i> link.<br />Select the <i>ALL</i> link.<br />Select the <i>monthly</i> link.<br />Select the <i>temperature</i> link."
LocationText8 = "gridded"
LocationHref1 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.temperature/"
--[[ Data By Searching --]]
LocationText9 = "ANACIM ENACTS version2 ALL daily temperature tmax"
LocationHref2 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.daily/.temperature/.tmax/"
LocationText10 = "temperature"
LocationHref3 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.daily/.temperature/"
LocationText11 = "ANACIM ENACTS version2 ALL daily temperature"

--[[Parameters for Tutoria/Datasets/Structure.--]]

StructureText1 = "ANACIM ENACTS version2 ALL daily temperature"
StructureHref1 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.daily/.temperature/"
StructureText2 = "temp"
--[[NB: /localconfig/figures/StructureFig2.png need be replaced.--]]
StructureText3 = "ENACTS"
StructureText4 = "daily ALL version2 ENACTS"
StructureText5 = "daily"
StructureText6 = "tmin"
StructureText7 = "longitude and latitude"
StructureText8 = "days since 1981-01-01 12:00:00"
StructureText9 = "1 Jan 1981"
StructureText10 = "20 Aug 2020"
StructureText11 = "14477"
StructureHref2 = "/SOURCES/.NOAA/.NCDC/.ERSST/.version4/"
StructureText12 = "SOURCES NOAA NCDC ERSST version4"

--[[Parameters for Tutorial/Selection/Station_Regions--]]

--[[ Gridded Data --]]
Stations_RegionsHref1 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/"
Stations_RegionsText1 = "ANACIM ENACTS v2 ALL monthly climatologies"
Stations_RegionsText2 = "15°W, 14°N"
Stations_RegionsText3 = "rfe"
Stations_RegionsText4 = "rfe"
Stations_RegionsHref2 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/rfe/"
Stations_RegionsText5 = "15W"
Stations_RegionsText6 = "14N"
Stations_RegionsHref3 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/rfe/"
Stations_RegionsText7 = "in the middle of Senegal"
Stations_RegionsText8 = "climatologies"
Stations_RegionsHref4 = "/expert/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/"
Stations_RegionsText9 = ".rfe"
Stations_RegionsHref5 = "/expert/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/.rfe/"
Stations_RegionsText10 = "17.5°-16°W, 14°-15.5°N"
Stations_RegionsText11 = "15.5N"
Stations_RegionsText12 = "14N"
Stations_RegionsText13 = "17.5W"
Stations_RegionsText14 = "16W"
Stations_RegionsText15 = "the central coast of Senegal"
Stations_RegionsHref6 = "/"
Stations_RegionsText16 = "15.5"
Stations_RegionsText17 = "14"
Stations_RegionsText18 = "-17.5"
Stations_RegionsText19 = "-16"

--[[Parameters for Tutorial/Selection/Data_Variables--]]

--[[ Single --]]
Data_VariablesText1 = "CDAS-1 monthly intrinsic"
Data_VariablesHref1 = "/SOURCES/.NOAA/.NCEP-NCAR/.CDAS-1/.MONTHLY/.Intrinsic/"
Data_VariablesText2 = "Pressure level zonal wind"
Data_VariablesText3 = "Select the <i>PressureLevel</i> link under the Datasets and variables heading.<br />Select the <i>zonal wind</i> link under the Datasets and variables heading."
Data_VariablesText4 = "PressureLevel zonal wind"
Data_VariablesHref2 = ".PressureLevel/.u/"
Data_VariablesText6 = ".PressureLevel .u"
Data_VariablesHref3 = "/expert"
--[[ Multiple --]]
Data_VariablesText7 = "ANACIM ENACTS v2 ALL monthly"
Data_VariablesHref4 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/"
Data_VariablesText8 = "monthly climatological rainfall"
Data_VariablesText9 = "monthly observed rainfall"
Data_VariablesHref5 = ".climatologies/.rfe/"
Data_VariablesHref6 = ".rainfall/.rfe"
Data_VariablesText10 = "SOURCES .ANACIM .ENACTS .version2 .ALL .monthly .climatologies .rfe"

--[[Parameters for Tutorial/Selection/Time_Period--]]

--[[ Continuous --]]
Time_PeriodHref1 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.dekadal/.rainfall/"
Time_PeriodText1 = "ENACTS v2 ALL dekadal rainfall"
Time_PeriodText2 = "November 17, 1982 - December 29, 2001"
Time_PeriodText3 = "Merged Station-Satellite Rainfall"
Time_PeriodHref2 = ".rfe/"
Time_PeriodText4 = "17 Nov 1982 to 29 Dec 2001"
Time_PeriodHref3 = "17+Nov+1982+to+29+Dec+2001"
Time_PeriodText5 = "Note that the limits of the time grid in the Data Selection box match the closest Wednesday to your selected time limits as dekad data are encompassing about 10 days."
Time_PeriodHref4 = "%2817%20Nov%201982%29%2829%20Dec%202001%29RANGE/"
Time_PeriodText6 = ".rfe"
Time_PeriodText7 = "T (17 Nov 1982) (29 Dec 2001) RANGE"
--[[ Discontinuous --]]
Time_PeriodHref5 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.daily/.temperature/"
Time_PeriodText8 = "ENACTS v2 ALL daily maximum temperature"
Time_PeriodHref6 = ".tmax/"

--[[Parameters for Tutorial/MVD/Manipulation--]]

--[[ Arithmetic --]]
MVDparameterText1 = "C"
MVDparameterText2 = "a gridbox (15°W, 14°N) in Senegal"
MVDparameterHref1 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.daily/.temperature/"
MVDparameterText3 = "ANACIM ENACTS daily temperature"
MVDparameterText4 = ""
MVDparameterHref2 = "X/-15/VALUE/Y/14/VALUE/"
MVDparameterHref3 = ".tmin/"
MVDparameterText5 = "precipitation"
MVDparameterHref4 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/"
MVDparameterText6 = "ANACIM ENACTS monthly"
MVDparameterText7b = " (months since 01-Jan) periodic Jan to Dec by 1. N= 12 pts"
MVDparameterText7 = " (months since 1960-01-01) ordered (Jan 1981) to (Sep 2020) by 1.0 N= 477 pts"
MVDparameterText8 = "Rainfall Amount"
MVDparameterHref5 = ".climatologies/.rfe"
MVDparameterHref6 = ".rainfall/.rfe/yearly-anomalies/"
MVDparameterText9 = "15°W, 14°N"
MVDparameterText10 = "14N"
MVDparameterText11 = "15W"
MVDparameterHref7 = ".rainfall/.rfe"
MVDparameterText12 = "monthly precipitation"
MVDparameterText13 = "mm"
MVDparameterText14 = "cm"
MVDparameterText15 = "0.1"
MVDparameterHref8 = ".rainfall/.rfe/"
MVDparameterText16 = "mm"
MVDparameterText17 = "m"
MVDparameterText18 = "1000."
MVDparameterHref4b = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/"
MVDparameterText6b = "ANACIM ENACTS monthly"
MVDparameterHref7b = ".rainfall/.rfe/"
--[[ Limits --]]
MVDparameterText19 = "10"
MVDparameterText20 = "Celsius"
MVDparameterText21 = "10."
MVDparameterText22 = "gridbox (15°W, 14°N) in Senegal"
MVDparameterHref9 = "X/-15/VALUE/Y/14/VALUE/"
MVDparameterText23 = "30."
MVDparameterHref10 = ".tmax/"
MVDparameterText24 = "gridbox (15°W, 14°N) in Senegal"
MVDparameterHref11 = "X/-15/VALUE/Y/14/VALUE/"
MVDparameterText25 = "January 1st"
MVDparameterText26 = "minimum temperature"
MVDparameterText27 = "10"
MVDparameterText28 = "˚C"
MVDparameterHref12 = ".tmin/"
MVDparameterText29 = "˚C"
MVDparameterText30 = "10."
MVDparameterText31 = "gridbox (15°W, 14°N) in Senegal"
MVDparameterHref13 = "X/-15/VALUE/Y/14/VALUE/"
--[[ Averages --]]
MVDparameterText32 = "Senegal"
MVDparameterHref14 = ""
MVDparameterText41 = "minimum temperature"
MVDparameterHref3b = ".tmin/"
MVDparameterText33 = "Jan 1, 1981 - Oct 10, 2020"
MVDparameterText34 = "Jan 8, 1981 - Oct 3, 2020"
MVDparameterText35 = "gridbox (15°W, 14°N) in Senegal"
MVDparameterHref15 = "X/-15/VALUE/Y/14/VALUE/"
--[[ Statistical --]]
MVDparameterText36 = "a gridbox (15°W, 14°N) in Senegal"
MVDparameterText8c = "SST anomalies"
MVDparameterText37 = "ANACIM ENACTS monthly precipitation"
MVDparameterHref16 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/"
MVDparameterText38 = "the gridbox (15°W, 14°N) in Senegal"
MVDparameterHref17 = "X/-15/VALUE/Y/14/VALUE/%5BX/Y%5Daverage/"
MVDparameterHref18 = ".rainfall/.rfe/"
MVDparameterText39 = "SOURCES .NOAA .NCDC .ERSST .version4 .anom"
MVDparameterHref4c = "/SOURCES/.NOAA/.NCDC/.ERSST/.version4/"
MVDparameterHref6c = ".anom/"
MVDparameterText40 = "ANACIM ENACTS monthly precipitation"
MVDparameterHref19 = ".rainfall/.rfe/"

--[[Parameters for Tutorial/MVD/Visualization--]]

--[[ Color Map --]]
VisualizationHref1 = "/SOURCES/.NOAA/.NCDC/.ERSST/.version4/"
VisualizationText1 = "ERSST v4"
VisualizationHref2 = ".anom/"
--[[ Contour Map --]]
VisualizationText2 = "July"
VisualizationText3 = "ENACTS v2 ALL monthly climatology rainfall"
VisualizationText4 = ""
VisualizationText5 = "ENACTS v2 ALL monthly rainfall"
VisualizationHref3 = ".ANACIM/.ENACTS/.version2/.ALL/.monthly/"
VisualizationText6 = ""
VisualizationHref4 = ".climatologies/.rfe/T/%28Jul%29VALUES/"
VisualizationText8 = "20"
VisualizationText8b = "mm"
--[[ 1D plots --]]
VisualizationText9 = "rainfall rfe"
VisualizationText10 = "15°W, 14.5°N"
VisualizationHref5 = ".rainfall/.rfe/"
VisualizationHref6 = "Y/%2814.5N%29VALUES/X/%2815W%29VALUES/"
VisualizationText11 = "precip_colors"
VisualizationHref7 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.rainfall/.rfe/X/(15W)/VALUE/Y/(14.5N)/VALUE/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.temperature/.tmax/X/(15W)/VALUE/Y/(14.5N)/VALUE/T/fig-/colorbars2/-fig/#expert"
VisualizationText12 = "monthly rainfall and the color of the bars indicates maximum temperature"
VisualizationHref8 = "/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.temperature/.tmax/T/(Jul 1982)/VALUE/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.temperature/.tmin/T/(Jul 1982)/VALUE/SOURCES/.ANACIM/.ENACTS/.version2/.ALL/.monthly/.rainfall/.rfe/T/(Jul 1982)/VALUE/fig-/scattercolor/-fig/#expert"
VisualizationText13 = "tmin and tmax to scatter the Jul 1982 data points while the color of each poin represents rainfall"
--[[ Vectors plots --]]
--[[ Cross Section plots --]]
--[[ Animation --]]
VisualizationText14 = "monthly"
VisualizationHref9 = ".sst/"
--[[ Colorscale --]]
VisualizationText15 = "ENACTS v2 ALL monthly temperature anomaly"
VisualizationText16 = "ENACTS v2 ALL monthly temperature anomaly calculated here for you"
VisualizationHref10 = ".ANACIM/.ENACTS/.version2/.ALL/.monthly/temperature/.tmax/climatologies/.tmax/sub/"
--[[ Land Mask --]]
--[[ MJO --]]

--[[Parameters for Tutorial/MVD/Download--]]
DownloadText1 = "ENACTS v2 ALL monthly rainfall climatologies"
DownloadHref1 = ".ANACIM/.ENACTS/.version2/.ALL/.monthly/.climatologies/.rfe/"
DownloadText2 = "rainfall climatology"
DownloadText3 = "1009584"
