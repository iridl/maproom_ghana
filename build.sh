#!/bin/sh -e
target="${1:-tarball}"
config="${2:-config.lua}"
. ./configure.sh "$config"
make $target
